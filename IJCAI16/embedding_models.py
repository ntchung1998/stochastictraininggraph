import torch
import torch.nn as nn
from graphsage.preps import prep_lookup

import numpy as np
import torch.nn.functional as F


from IJCAI16.loss import EmbeddingLossFunctions, MappingLossFunctions

def fixed_unigram_candidate_sampler(num_sampled, unique, range_max, distortion, unigrams):
    weights = unigrams**distortion
    prob = weights/weights.sum()
    sampled = np.random.choice(range_max, num_sampled, p=prob, replace=~unique)
    return sampled

############################## PALE EMBEDDING #############################3
class PaleEmbedding(nn.Module):
    def __init__(self, n_nodes, embedding_dim, deg, neg_sample_size, cuda, neg_weight):

        """
        Parameters
        ----------
        n_nodes: int
            Number of all nodes
        embedding_dim: int
            Final embedding_dim for nodes
        deg: ndarray , shape = (-1,)
            Array of degrees of all nodes
        neg_sample_size : int
            Number of negative candidate to sample
        cuda: bool
            Whether to use cuda
        neg_weight: float
            Used to define value of neg_sample_weights of embedding loss function
        """

        super(PaleEmbedding, self).__init__()
        self.node_embedding = nn.Embedding(n_nodes, embedding_dim)
        self.deg = deg
        self.neg_sample_size = neg_sample_size
        self.link_pred_layer = EmbeddingLossFunctions(loss_fn='xent', neg_sample_weights=neg_weight)
        self.n_nodes = n_nodes
        self.use_cuda = cuda


    def loss(self, nodes, neighbor_nodes):
        batch_output, neighbor_output, neg_output = self.forward(nodes, neighbor_nodes)
        batch_size = batch_output.shape[0]
        loss, loss0, loss1 = self.link_pred_layer.loss(batch_output, neighbor_output, neg_output)
        loss = loss/batch_size
        loss0 = loss0/batch_size
        loss1 = loss1/batch_size
        return loss, loss0, loss1


    def forward(self, nodes, neighbor_nodes=None):
        """
        Parameters:
        ----------
        nodes: torch.Tensor or torch.Tensor.cuda()
            List of nodes for getting embedding
        neighbor_nodes: torch.Tensor or torch.Tensor.cuda(), optional
            List of nodes which have edges connect to nodes. If None, forward is just used for 
            getting nodes embedding when training had finished.

        Returns
        ----------
        torch.Tensor or torch.Tensor.cuda()
        * if neighbor_nodes is not None:
            Current embedding values of: nodes, neighbor_nodes, and neg_nodes
        * else:
            Embedding of nodes
        """

        # embedding...
        node_output = self.node_embedding(nodes)
        node_output = F.normalize(node_output, dim=1)

        if neighbor_nodes is not None:
            # sample negative nodes
            neg = fixed_unigram_candidate_sampler(
                num_sampled=self.neg_sample_size,
                unique=False,
                range_max=len(self.deg),
                distortion=0.75,
                unigrams=self.deg
                )

            neg = torch.LongTensor(neg)
            
            if self.use_cuda:
                neg = neg.cuda()
            neighbor_output = self.node_embedding(neighbor_nodes)
            neg_output = self.node_embedding(neg)
            # normalize
            neighbor_output = F.normalize(neighbor_output, dim=1)
            neg_output = F.normalize(neg_output, dim=1)

            return node_output, neighbor_output, neg_output

        return node_output

    def get_embedding(self):
        # Returns: embedding as lookup tables for all nodes
        nodes = np.arange(self.n_nodes)
        nodes = torch.LongTensor(nodes)
        if self.use_cuda:
            nodes = nodes.cuda()
        embedding = None
        BATCH_SIZE = 512
        
        for i in range(0, self.n_nodes, BATCH_SIZE):
            j = min(i + BATCH_SIZE, self.n_nodes)
            batch_nodes = nodes[i:j]
            if batch_nodes.shape[0] == 0: break
            batch_node_embeddings = self.forward(batch_nodes)
            if embedding is None:
                embedding = batch_node_embeddings
            else:
                embedding = torch.cat((embedding, batch_node_embeddings))

        return embedding


################# GRAPHSAGE EMBEDDING ###########################
# class SampleAndAggregate(nn.Module):
#     def __init__(self, deg, sampler, n_samples, agg_layers, features=None, cuda=False, identity_dim=0):

#         """
#         Parameters
#         ----------
#         deg: ndarray , shape = (-1,)
#             Array of degrees of all nodes
#         sempler: class Sample
#             Type of sample strategy
#         n_samples: list
#             List of number of neighbors to be sampled at each layer
#         agg_layers: list
#             List of agg layers
#         features: torch.Tensor()
#             Init_embedding for nodes as features
#         cuda: bool
#             Whether to use cuda (GPU)
#         """

#         super(SampleAndAggregate, self).__init__()
#         self.deg = deg
#         self.sample_fn = sampler
#         self.agg_layers = nn.Sequential(*agg_layers)
#         self.num_aggs = len(agg_layers)
#         self.n_samples = n_samples
#         self.n_nodes = len(deg)
#         self.features = features
#         self.use_cuda = cuda

#         if features is not None:
#             input_dim = features.shape[1]
#         else:
#             input_dim = 0

#         if identity_dim > 0:
#             print("Use NodeEmbedding Preprocessing!")
#             self.prep = prep_lookup['use_identity_features'](input_dim=input_dim, n_nodes=deg.shape[0], identity_dim=identity_dim)
#         else:
#             if input_dim == 0:
#                 raise Exception("Must have a positive value for identity feature dimension if no input features given.")
#             print("Use Identity Preprocessing!")
#             self.prep = prep_lookup['use_original_features'](input_dim=input_dim)


#     def sample(self, inputs):
#         """
#         Parameters
#         ----------
#         inputs: torch.Tensor or torch.Tensor.cuda()
#             List of nodes for sampling neighbors

#         Returns
#         ----------
#         list
#             list of length K, samples[K-k-1] is the samples from k-hop neighbors of batch inputs
#         """


#         samples = [inputs]
#         for k in range(self.num_aggs):
#             # Aggregator for k-hop neighbors is at layer K-k
#             t = self.num_aggs - k - 1
#             sampler = self.sample_fn
#             nodes = sampler.sample(samples[k], self.n_samples[t]) 
#             nodes = nodes.contiguous().view(-1) #Flatten
#             samples.append(nodes)        
#         return samples

#     def aggregate(self, samples):
#         """
#         Parameters:
#         ----------
#         samples: list of torch.Tensor or torch.Tensor.cuda()
#             A list of samples of variable hops away for convolving at each layer of the
#             network. Length is the number of layers + 1. Each is a vector of node indices

#         Returns:
#         ----------
#         torch.Tensor of torch.Tensor.cuda()
#             The hidden representation at the final layer for all nodes in batch
#         """
#         has_feats = self.features is not None
#         if has_feats:
#             tmp_feats = [self.features[samples[i]] for i in range(len(samples))]
#         else:
#             tmp_feats = [None]*len(samples)
#         hidden = [self.prep(samples[i], tmp_feats[i], hop_idx=len(samples) - i - 1) for i in range(len(samples))]

#         # hidden = [self.features[samples[i]] for i in range(len(samples))]
#         layer = 0
#         for aggregator in self.agg_layers.children():
#             next_hidden = []
#             for hop in range(self.num_aggs - layer):
#                 h = aggregator(hidden[hop], hidden[hop + 1])
#                 next_hidden.append(h)
#             hidden = next_hidden
#             layer = layer + 1
            
#         return hidden[0]


# class GraphsageEmbedding(SampleAndAggregate):

#     def __init__(self, deg, sampler, n_samples, agg_layers, features, neg_sample_size, cuda, identity_dim):

#         super(GraphsageEmbedding, self).__init__(deg, sampler, n_samples, agg_layers, features, cuda, identity_dim)
#         self.link_pred_layer = EmbeddingLossFunctions()
#         self.neg_sample_size = neg_sample_size

#     def loss(self, inputs1, inputs2):
#         batch_size = inputs1.size()[0]
#         outputs1, outputs2, neg_outputs  = self.forward(inputs1, inputs2)
#         batch_size = outputs1.shape[0]

#         loss, loss0, loss1 = self.link_pred_layer.loss(outputs1, outputs2, neg_outputs)
#         loss = loss/batch_size
#         loss0 = loss0/batch_size
#         loss1 = loss1/batch_size
#         return loss, loss0, loss1

#     def forward(self, inputs1, inputs2 = None):
#         """
#         Parameters:
#         ----------
#         inputs1: torch.Tensor or torch.Tensor.cuda()
#             List of nodes for getting embedding
#         inputs2: torch.Tensor or torch.Tensor.cuda(), optional
#             List of nodes which co-occur with inputs1s random_walk. If None, forward is just used for 
#             getting inputs1 embedding when training had finished.

#         Returns
#         ----------
#         torch.Tensor or torch.Tensor.cuda()
#         * if neighbor_nodes is not None:
#             Current embedding values of: inputs1, inputs2, and neg_nodes used for getting loss
#         * else:
#             Embedding of inputs1
#         """

#         samples1 = self.sample(inputs1)
#         outputs1 = self.aggregate(samples1)
#         outputs1 = F.normalize(outputs1, dim=1)

#         if inputs2 is not None:
#             # get negative candidates
#             neg = fixed_unigram_candidate_sampler(
#                 num_sampled=self.neg_sample_size,
#                 unique=False,
#                 range_max=len(self.deg),
#                 distortion=0.75,
#                 unigrams=self.deg
#             )

#             neg = torch.LongTensor(neg)
#             if self.use_cuda:
#                 neg = neg.cuda()
#             # sample neighbors for neg
#             neg_samples = self.sample(neg)

#             samples2 = self.sample(inputs2)
#             outputs2 = self.aggregate(samples2)
#             neg_outputs = self.aggregate(neg_samples)
#             # normalize
#             outputs2 = F.normalize(outputs2, dim=1)
#             neg_outputs = F.normalize(neg_outputs, dim=1)

#             return outputs1, outputs2, neg_outputs

#         return outputs1

#     def get_embedding(self):
#         # Returns: embedding as lookup tables for all nodes
#         nodes = np.arange(self.n_nodes)
#         nodes = torch.LongTensor(nodes)
#         if self.use_cuda:
#             nodes = nodes.cuda()
#         embedding = None
#         BATCH_SIZE = 512
        
#         for i in range(0, self.n_nodes, BATCH_SIZE):
#             j = min(i + BATCH_SIZE, self.n_nodes)
#             batch_nodes = nodes[i:j]
#             if batch_nodes.shape[0] == 0: break
#             batch_node_embeddings = self.forward(batch_nodes)
#             if embedding is None:
#                 embedding = batch_node_embeddings
#             else:
#                 embedding = torch.cat((embedding, batch_node_embeddings))
#         return embedding