# from IJCAI16.embedding_models import GraphsageEmbedding
from IJCAI16.mapping_model import PaleMappingLinear, PaleMappingMlp
from IJCAI16.dataset import Dataset
from IJCAI16.utils import draw_result, save_embeddings, to_word2vec_format, translation, mapping_train_, read_emb
from graphsage.unsupervised_models import UnsupervisedGraphSage

import argparse
import os
import time

import numpy as np
from copy import deepcopy

import torch
import torch.nn.functional as F

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import random

from graphsage.neigh_samplers import UniformNeighborSampler
from graphsage.aggregators import MeanAggregator

def parse_args():
    parser = argparse.ArgumentParser(description="Run supervised graphSAGE")
    parser.add_argument('--prefix1',             default="$HOME/dataspace/graph/pale_facebook/random_clone/sourceclone,alpha_c=0.9,alpha_s=0.9/pale_facebook")
    parser.add_argument('--prefix2',             default="$HOME/dataspace/graph/pale_facebook/random_clone/sourceclone,alpha_c=0.9,alpha_s=0.9/pale_facebook")
    
    parser.add_argument('--base_log_dir',        default='$HOME/dataspace/IJCAI16_results')
    parser.add_argument('--cuda',                action='store_true')
    parser.add_argument('--log_name',            default='pale_facebook')


    # embedding dim, try: 80, 100, 200, 300, 800 | neg_sample_size, try: 10, 5, 15, 20
    parser.add_argument('--learning_rate1',      default=0.05,        type=float)
    parser.add_argument('--batch_size_embedding',default=128,         type=int)
    parser.add_argument('--embedding_epochs',    default=1000,        type=int)
    parser.add_argument('--neg_sample_size',     default=20,          type=int)
    parser.add_argument('--embedding_model',     default='graphsage_mean')
    parser.add_argument('--neg_weight',          default=1.0,         type=float)
    parser.add_argument('--train_dict',          default=None)    
    parser.add_argument('--val_dict',            default=None)

    parser.add_argument('--learning_rate2',      default=0.001,       type=float)
    parser.add_argument('--batch_size_mapping',  default=128,         type=int)
    parser.add_argument('--mapping_epochs',      default=500,         type=int)
    parser.add_argument('--mapping_model',       default='linear')
    parser.add_argument('--train_percent',       default=0.03,        type=float)
    parser.add_argument('--activate_function',   default='sigmoid') # for MLP mapping

    # FOR LOAD EMBEDDING
    parser.add_argument('--embedding_dir1',   default=None) 
    parser.add_argument('--embedding_dir2',   default=None) 
    parser.add_argument('--max_iter',         default=1000000000, type=int)

    # ONLY USE FOR GRAPHSAGE EMBEDDING
    parser.add_argument('--samples_1',        default=25,              help='Number of samples in layer 1', type=int)
    parser.add_argument('--samples_2',        default=10,              help='Number of samples in layer 2', type=int)
    parser.add_argument('--out_dim',          default=128,             help='Size of output dim (final is 2x this, if using concat)', type=int)
    parser.add_argument('--concat',           action='store_false') 
    parser.add_argument('--dropout',          default=0.0,             help='Dropout rate (1 - keep probability).', type=float)
    parser.add_argument('--max_degree',       default=25,         type=int)
    parser.add_argument('--identity_dim',     default=50, type=int) 
    parser.add_argument('--use_features',     action='store_true')  
    parser.add_argument('--n_layer',          default=1, type=int)
    
    return parser.parse_args()


def embedding_train_(model, edges, optimizer, args):
    # batch size and n_iters
    batch_size = args.batch_size_embedding
    n_iters = len(edges)//batch_size
    assert n_iters > 0, "Batch size is too large!"
    if(len(edges) % batch_size > 0):
        n_iters = n_iters + 1
    print_every = int(n_iters/4) + 1
    total_steps = 0
    n_epochs = args.embedding_epochs

    for epoch in range(1, n_epochs+1):
        st = time.time()
        print("Epoch {0}".format(epoch))
        np.random.shuffle(edges)
        for iter in range(n_iters):
            batch_edges = torch.LongTensor(edges[iter*batch_size:(iter+1)*batch_size])
            if args.cuda:
                batch_edges = batch_edges.cuda()
            start_time = time.time()
            optimizer.zero_grad()
            loss, _, _, _ = model.loss(batch_edges[:,0], batch_edges[:,1])
            loss.backward()
            optimizer.step()

            if total_steps % print_every == 0:
                print("Iter:", '%03d' %iter,
                      "train_loss=", "{:.5f}".format(loss.item()),
                      "time", "{:.5f}".format(time.time()-start_time)
                      )
            total_steps += 1
            if total_steps > args.max_iter:
                break
        if total_steps > args.max_iter:
            break
        print("Epoch time: ", time.time() - st)



def train_embedding(embedding, edges, optimizer, args):
    embedding_train_(embedding, edges, optimizer, args)
    embeddings_trained = save_embeddings(embedding)
    embeddings_trained = torch.FloatTensor(embeddings_trained)
    if args.cuda:
        embeddings_trained = embeddings_trained.cuda()
    return embeddings_trained



def train(data, args):
    # args.log_name = "st_vs_st_with_feature"
    args.log_name +=  "_train_percent" + str(args.train_percent) + '_GRAPHSAGE' 
    ###################################################################################33
    # DATA
    ###################################################################################
    source_train_nodes = deepcopy(data.source_train_node_idx)
    source_val_nodes = deepcopy(data.source_val_node_idx)
    target_train_nodes = deepcopy(data.target_train_node_idx)
    target_val_nodes = deepcopy(data.target_val_node_idx)

    stdict = deepcopy(data.stdict)

    source_id2idx = data.source_id2idx
    target_id2idx = data.target_id2idx

    deg1 = deepcopy(data.source_deg) 
    deg2 = deepcopy(data.target_deg)

    source_edges = deepcopy(data.source_edges)
    target_edges = deepcopy(data.target_edges)

    print("number of source edges: ", len(source_edges))
    print("number of target edges: ", len(target_edges))

    source_val_nodes = [node for node in source_val_nodes if node in list(stdict.keys())]


    ######################################################################################
    # EMBEDDING MODEL
    ######################################################################################

    ############# Load embedding: if embedding_dir is not None 

    if args.embedding_dir1 is not None:
        if not os.path.exists(args.embedding_dir1):
            raise Exception("haven't had embedding yet!!!")
        srcfile = open(args.embedding_dir1, encoding='utf-8', errors='surrogateescape')
        trgfile = open(args.embedding_dir2, encoding='utf-8', errors='surrogateescape')
        embeddings_trained1 = read_emb(srcfile, source_id2idx)
        embeddings_trained2 = read_emb(trgfile, target_id2idx)

        embeddings_trained1 = torch.FloatTensor(embeddings_trained1)
        embeddings_trained2 = torch.FloatTensor(embeddings_trained2)
        if args.cuda:
            embeddings_trained1 = embeddings_trained1.cuda()
            embeddings_trained2 = embeddings_trained2.cuda()

        embedding_dim = args.out_dim * 2


    ############# Else: define embedding

    else:
        print("Use GraphsageEmbedding")
        # with graphsage, we may want to use features or identitydim or both
        source_features = deepcopy(data.source_features)
        target_features = deepcopy(data.target_features)
        if source_features is not None:
            source_features = torch.FloatTensor(source_features)
            target_features = torch.FloatTensor(target_features)
            if args.cuda:
                source_features = source_features.cuda()
                target_features = target_features.cuda()
            print(source_features.shape)
            init_dim = source_features.shape[1] + args.identity_dim
        else:
            init_dim = args.identity_dim
        print("features dim: ", init_dim)
        # aggregators
        aggregator_cls = MeanAggregator

        agg11 = aggregator_cls(input_dim=init_dim, output_dim=args.out_dim, activation=False, concat=args.concat, dropout=args.dropout)
        agg_layers1 = [agg11]

        agg12 = aggregator_cls(input_dim=init_dim, output_dim=args.out_dim, activation=False, concat=args.concat, dropout=args.dropout)
        agg_layers2 = [agg12]
        n_samples = [args.samples_1]

        adj1 = torch.LongTensor(deepcopy(data.source_adj))
        adj2 = torch.LongTensor(deepcopy(data.target_adj))

        if args.cuda:
            adj1 = adj1.cuda()
            adj2 = adj2.cuda()

        embedding1 = UnsupervisedGraphSage(
                                    features = source_features,
                                    train_adj = None,
                                    adj = adj1,
                                    train_deg = None,
                                    deg = deg1,
                                    agg_layers=agg_layers1,
                                    n_samples=n_samples,
                                    sampler=UniformNeighborSampler(adj1),
                                    fc=False,
                                    identity_dim=args.identity_dim,
                                    neg_sample_size=args.neg_sample_size,
                                    use_cuda=args.cuda)


        embedding2 = UnsupervisedGraphSage(
                                    features = target_features,
                                    train_adj = None,
                                    adj = adj2,
                                    train_deg = None,
                                    deg = deg2,
                                    agg_layers=agg_layers2,
                                    n_samples=n_samples,
                                    sampler=UniformNeighborSampler(adj2),
                                    fc=False,
                                    identity_dim=args.identity_dim,
                                    neg_sample_size=args.neg_sample_size,
                                    use_cuda=args.cuda)


        if args.cuda:
            embedding1 = embedding1.cuda()
            embedding2 = embedding2.cuda()
        optimizer_e1 = torch.optim.Adam(filter(lambda p : p.requires_grad, embedding1.parameters()), lr=args.learning_rate1)
        optimizer_e2 = torch.optim.Adam(filter(lambda p : p.requires_grad, embedding2.parameters()), lr=args.learning_rate1)

        print("Start training for source embedding")
        embeddings_trained1 = train_embedding(embedding1, source_edges, optimizer_e1, args)


        to_word2vec_format(embeddings_trained1, data.source_node_ids, args.base_log_dir + "/embeddings/" \
            + args.log_name + "source.emb", dim = len(embeddings_trained1[0]), id2idx=source_id2idx)

        
        print("Start training for target embedding")        
        embeddings_trained2 = train_embedding(embedding2, target_edges, optimizer_e2, args)


        to_word2vec_format(embeddings_trained2, data.target_node_ids, args.base_log_dir + "/embeddings/" \
            + args.log_name + "target.emb", dim = len(embeddings_trained2[0]), id2idx=target_id2idx)


    #######################################################################################
    # MAPPING MODEL
    #######################################################################################
    embedding_dim = args.out_dim * 2
    if args.mapping_model == 'linear':
        print("Use linear mapping")
        mapping_model = PaleMappingLinear(
                                    embedding_dim=embedding_dim,
                                    source_embedding=embeddings_trained1,
                                    target_embedding=embeddings_trained2,
                                    )
    else:
        print("Use Mpl mapping")
        mapping_model = PaleMappingMlp(
                                    embedding_dim=embedding_dim,
                                    source_embedding=embeddings_trained1,
                                    target_embedding=embeddings_trained2,
                                    activate_function=args.activate_function
                                    )

    if args.cuda:
        mapping_model = mapping_model.cuda()

    optimizer_m = torch.optim.Adam(filter(lambda p : p.requires_grad, mapping_model.parameters()), lr=args.learning_rate2)

    print("Start training for mapping function")
    train_losses, val_losses = mapping_train_(mapping_model, source_train_nodes, source_val_nodes, stdict, optimizer_m, args, data)

    # draw_result(train_losses, val_losses, None, args, 'sam_' + args.log_name)

    ########################################################################################
    # EVALUATE
    ########################################################################################
    # if load_embedding == 0:
        # save_embeddings(embedding1, 0, args, mapping_model, name='sam_' + args.log_name)
    source_after_mapping = mapping_model(embeddings_trained1)
    to_word2vec_format(source_after_mapping, data.source_node_ids, args.base_log_dir + "/embeddings/" + args.log_name + "source_after_mapping.emb", dim = len(source_after_mapping[0]), id2idx=source_id2idx)

    train_acc, val_acc = translation(source_after_mapping, embeddings_trained2, source_train_nodes, stdict)
    print("Train_acc: ", train_acc, " Val_acc: ", val_acc)


    if not os.path.exists(args.base_log_dir + '/results'):
        os.makedirs(args.base_log_dir + '/results')

    with open(args.base_log_dir + '/results/' + args.log_name + '.txt', 'w') as f:
        f.write("Train_acc: %s, Val_acc: %s \n" %(train_acc, val_acc))

    
    print("result has been saved to: ", args.base_log_dir + '/results/' + args.log_name + '.txt')


if __name__ == "__main__":
    args = parse_args()
    print(args)
    seed = 123
    np.random.seed(seed)
    random.seed(seed)
    torch.manual_seed(seed)

    data = Dataset(args.prefix1, args.prefix2, args.embedding_model, args.use_features, args.train_dict, args.val_dict, args.max_degree)

    train(data, args)


