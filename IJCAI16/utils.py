import os
import numpy as np
import torch
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import torch.nn.functional as F
import random
import time



def to_word2vec_format(val_embeddings, nodes, output_file_name, dim, id2idx, pref=""):
    val_embeddings = val_embeddings.cpu().detach().numpy()
    with open(output_file_name, 'w') as f_out:
        f_out.write("%s %s\n"%(len(nodes), dim))
        for i, node in enumerate(nodes):
            txt_vector = ["%s" % val_embeddings[int(id2idx[node])][j] for j in range(dim)]
            f_out.write("%s%s %s\n" % (pref, node, " ".join(txt_vector)))
        f_out.close()
    print("emb has been saved to: ", output_file_name)


def read_emb(file, id2idx,dtype='float'):
    header = file.readline().split(' ')
    count = int(header[0])
    dim = int(header[1])
    matrix = np.empty((count, dim), dtype=dtype)
    for i in range(count):
        word, vec = file.readline().split(' ', 1)
        matrix[id2idx[word]] = np.fromstring(vec, sep=' ', dtype=dtype)
    return matrix



def embedding_train_(model, edges, optimizer, args):
    losses = []
    losses0 = []
    losses1 = []
    # batch size and n_iters
    batch_size = args.batch_size_embedding
    n_iters = len(edges)//batch_size
    assert n_iters > 0, "Batch size is too large!"
    if(len(edges) % batch_size > 0):
        n_iters = n_iters + 1
    print_every = int(n_iters/4) + 1
    total_steps = 0
    n_epochs = args.embedding_epochs

    for epoch in range(1, n_epochs+1):
        st = time.time()
        # print("Epoch {0}".format(epoch))
        np.random.shuffle(edges)
        for iter in range(n_iters):
            batch_edges = torch.LongTensor(edges[iter*batch_size:(iter+1)*batch_size])
            if args.cuda:
                batch_edges = batch_edges.cuda()
            start_time = time.time()
            optimizer.zero_grad()
            loss, loss0, loss1 = model.loss(batch_edges[:,0], batch_edges[:,1])
            loss.backward()
            optimizer.step()

            if total_steps % print_every == 0:
                losses.append(loss.item())
                losses0.append(loss0.item())
                losses1.append(loss1.item())
                # print("Iter:", '%03d' %iter,
                #       "train_loss=", "{:.5f}".format(loss.item()),
                #       "true_loss=", "{:.5f}".format(loss0.item()),
                #       "neg_loss=", "{:.5f}".format(loss1.item()), 
                #       "time", "{:.5f}".format(time.time()-start_time)
                #       )
            total_steps += 1
            if total_steps > args.max_iter:
                break
        if total_steps > args.max_iter:
            break
        if epoch % 250 == 0:
            print("Epoch time: ", time.time() - st)
    losses = np.array(losses)
    losses0 = np.array(losses0)
    losses1 = np.array(losses1)

    return losses, losses0, losses1


def train_embedding(embedding, edges, optimizer, args):
    losses, losses0, losses1 = embedding_train_(embedding, edges, optimizer, args)
    # draw_result(losses/5, losses0, losses1/5, args, train_for + args.log_name)
    embeddings_trained = save_embeddings(embedding)
    embeddings_trained = torch.FloatTensor(embeddings_trained)
    if args.cuda:
        embeddings_trained = embeddings_trained.cuda()
    return embeddings_trained


def mapping_train_(model, source_train_nodes, source_val_nodes, stdict, optimizer, args, data):
    train_losses = []
    val_losses = []
    target_val_nodes = [stdict[x] for x in source_val_nodes]
    source_val_nodes = torch.LongTensor(source_val_nodes)
    target_val_nodes = torch.LongTensor(target_val_nodes)
    if args.cuda:
        source_val_nodes = source_val_nodes.cuda()
        target_val_nodes = target_val_nodes.cuda()
    batch_size = args.batch_size_mapping

    n_iters = len(source_train_nodes)//batch_size
    assert n_iters > 0, "batch_size is too large"
    if(len(source_train_nodes) % batch_size > 0):
        n_iters = n_iters + 1
    print_every = int(n_iters/4) + 1
    total_steps = 0
    n_epochs = args.mapping_epochs
    for epoch in range(1, n_epochs+1):
        # print("Epoch {0}".format(epoch))
        np.random.shuffle(source_train_nodes)
        for iter in range(n_iters):
            source_batch = source_train_nodes[iter*batch_size:(iter+1)*batch_size]
            target_batch = [stdict[x] for x in source_batch]
            source_batch = torch.LongTensor(source_batch)
            target_batch = torch.LongTensor(target_batch)
            if args.cuda:
                source_batch = source_batch.cuda()
                target_batch = target_batch.cuda()
            optimizer.zero_grad()
            start_time = time.time()
            loss = model.loss(source_batch, target_batch)
            loss.backward()
            optimizer.step()
            if total_steps % print_every == 0 and total_steps > 0:
                val_loss = model.loss(source_val_nodes, target_val_nodes)
                val_losses.append(val_loss.item())
                train_losses.append(loss.item())
                # print("Iter:", '%03d' %iter,
                #       "train_loss=", "{:.5f}".format(loss.item()),
                #       "val_loss", "{:.5f}".format(val_loss),
                #       "time", "{:.5f}".format(time.time()-start_time)
                #       )
                del val_loss
            total_steps += 1
    print("min: ", round(min(val_losses),2), "max: ", round(max(val_losses),2), "mean: ", round(np.mean(val_losses),2))
    return train_losses, val_losses


def draw_result(losses, losses0=None, losses1=None, args=None, name=None):
    fig, ax = plt.subplots(figsize=(20,10))
    lw = 2
    if losses1 is None:
        label0 = "train loss"
        label1 = "val loss"
    else:
        label0 = "total loss"
        label1 = "true loss"
        label2 = "neg loss"
    plt.plot(np.arange(len(losses)), losses, color='darkorange', label = label0)
    if losses0 is not None:
        plt.plot(np.arange(len(losses0)), losses0, color='navy', lw=lw, label = label1)
    if losses1 is not None:
        plt.plot(np.arange(len(losses1)), losses1, color='black', lw=lw, label = label2)

    plt.xlabel('iteration')
    plt.ylabel('loss value')
    plt.title(name)
    plt.legend()
    plt.grid(True)
    plt.savefig(args.base_log_dir + '/plot_losses/' + name + ".png")


def log_dir(args):
    log_dir = args.base_log_dir + "/" + args.prefix1.split("/")[-1] + "/"
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    return log_dir



def save_embeddings(model, mapping_model=None):
    embeddings = model.get_embedding()
    n_nodes = len(embeddings)
    if mapping_model is not None:
        BATCH_SIZE = 512
        mapping_embeddings = None
        for i in range(0, n_nodes, BATCH_SIZE):
            j = min(i + BATCH_SIZE, n_nodes)
            batch_embeddings = embeddings[i:j]
            if batch_embeddings.shape[0] == 0: break
            batch_mapping = mapping_model(batch_embeddings)
            if mapping_embeddings is None:
                mapping_embeddings = batch_mapping
            else:
                mapping_embeddings = torch.cat((mapping_embeddings, batch_mapping))
        embeddings = mapping_embeddings
    embeddings = embeddings.cpu().detach().numpy()
    return embeddings



def translation(x, z, source_train_nodes, ground_truth):
    """
    Parameters
    ---------
    x: torch.FloatTensor or torch.FloatTensor.cuda
        Source embedding for all nodes, x[i, :] is the embedding vector of source
        nodes with idx equal to i
    z: torch.FloatTensor or torch.FloatTensor.cuda
        Target embedding for all nodes, z[i, :] is the embedding vector of target
        nodes with idx equal to i
    source_train_nodes: iterable, for example: list
        List of idxs of all train nodes
    ground_truth: dict
        Dictionary as groundtruth maps source node idxs to target node idxs
    neibs_dict: dict, optional
        Dictionary of neighbors of each target node idx that will be used for determine
        how many percent of wrong predict lies on neighbors of target node.

    Returns
    ---------
    float, float
        Train accuracy and Val accuracy
    """    
    BATCH_SIZE = 500

    n_train = len(source_train_nodes)
    count_train = 0
    count_val = 0

    for i in range(0, len(x), BATCH_SIZE):
        j = min(i + BATCH_SIZE, len(x))
        similarities = torch.matmul(x[i:j], z.t()).argmax(dim=1).tolist()
        for k in range(len(similarities)):
            if i + k in list(ground_truth.keys()):
                if similarities[k] == ground_truth[i + k]:
                    if i + k in source_train_nodes:
                        count_train += 1
                    else:
                        count_val += 1
        print(count_train / n_train, count_val / (len(ground_truth) - n_train))
    train_acc = count_train / n_train
    val_acc = count_val / (len(ground_truth) - n_train)
    return train_acc, val_acc