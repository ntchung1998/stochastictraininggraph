BLD=/home/bigdata/thomas/dataspace/IJCAI16_results
DS2=/home/bigdata/thomas/dataspace/graph/ppi/sub_graph/subgraph3/random_clone/del_edge,p=
LOGNAME=sub_vs_sub_del_edge_feat100
TRAIN_RATIO=0.2
RATIO=0.1

time python vecmap/map_embeddings.py --semi_supervised ${DS2}${RATIO},n=${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.train.dict \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource_vecmap.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget_vecmap.emb \
    --cuda

python vecmap/eval_translation.py \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource_vecmap.emb \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget_vecmap.emb \
-d ${DS2}${RATIO},n=${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.test.dict \
--retrieval nn --cuda > ${LOGNAME}_${TRAIN_RATIO}_vecmap 


TRAIN_RATIO=0.03


time python vecmap/map_embeddings.py --semi_supervised ${DS2}${RATIO},n=${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.train.dict \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource_vecmap.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget_vecmap.emb \
    --cuda

python vecmap/eval_translation.py \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource_vecmap.emb \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget_vecmap.emb \
-d ${DS2}${RATIO},n=${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.test.dict \
--retrieval nn --cuda > ${LOGNAME}_${TRAIN_RATIO}_vecmap 





BLD=/home/bigdata/thomas/dataspace/IJCAI16_results
DS2=/home/bigdata/thomas/dataspace/graph/ppi/sub_graph/subgraph3/random_clone/add_edge,p=
LOGNAME=sub_vs_sub_add_edge_feat_iden100
TRAIN_RATIO=0.2
RATIO=0.1

time python vecmap/map_embeddings.py --semi_supervised ${DS2}${RATIO},n=${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.train.dict \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource_vecmap.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget_vecmap.emb \
    --cuda

python vecmap/eval_translation.py \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource_vecmap.emb \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget_vecmap.emb \
-d ${DS2}${RATIO},n=${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.test.dict \
--retrieval nn --cuda > ${LOGNAME}_${TRAIN_RATIO}_vecmap 



TRAIN_RATIO=0.03


time python vecmap/map_embeddings.py --semi_supervised ${DS2}${RATIO},n=${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.train.dict \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource_vecmap.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget_vecmap.emb \
    --cuda

python vecmap/eval_translation.py \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource_vecmap.emb \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget_vecmap.emb \
-d ${DS2}${RATIO},n=${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.test.dict \
--retrieval nn --cuda > ${LOGNAME}_${TRAIN_RATIO}_vecmap 


BLD=/home/bigdata/thomas/dataspace/IJCAI16_results
DS2=/home/bigdata/thomas/dataspace/graph/ppi/sub_graph/subgraph3/random_delete_node/del,p=
RATIO=0.1
LOGNAME=sub_vs_sub_del_node01_feat100

TRAIN_RATIO=0.2

time python vecmap/map_embeddings.py --semi_supervised ${DS2}${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.train.dict \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource_vecmap.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget_vecmap.emb \
    --cuda

python vecmap/eval_translation.py \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource_vecmap.emb \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget_vecmap.emb \
-d ${DS2}${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.test.dict \
--retrieval nn --cuda > ${LOGNAME}_${TRAIN_RATIO}_vecmap 

TRAIN_RATIO=0.03


time python vecmap/map_embeddings.py --semi_supervised ${DS2}${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.train.dict \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource_vecmap.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget_vecmap.emb \
    --cuda

python vecmap/eval_translation.py \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource_vecmap.emb \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget_vecmap.emb \
-d ${DS2}${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.test.dict \
--retrieval nn --cuda > ${LOGNAME}_${TRAIN_RATIO}_vecmap 




BLD=/home/bigdata/thomas/dataspace/IJCAI16_results
DS2=/home/bigdata/thomas/dataspace/graph/ppi/sub_graph/subgraph3/random_delete_node/del,p=
RATIO=0.2
LOGNAME=sub_vs_sub_del_node02_feat100

TRAIN_RATIO=0.2

time python vecmap/map_embeddings.py --semi_supervised ${DS2}${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.train.dict \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource_vecmap.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget_vecmap.emb \
    --cuda

python vecmap/eval_translation.py \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource_vecmap.emb \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget_vecmap.emb \
-d ${DS2}${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.test.dict \
--retrieval nn --cuda > ${LOGNAME}_${TRAIN_RATIO}_vecmap 

TRAIN_RATIO=0.03


time python vecmap/map_embeddings.py --semi_supervised ${DS2}${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.train.dict \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource_vecmap.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget_vecmap.emb \
    --cuda

python vecmap/eval_translation.py \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEsource_vecmap.emb \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_GRAPHSAGEtarget_vecmap.emb \
-d ${DS2}${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.test.dict \
--retrieval nn --cuda > ${LOGNAME}_${TRAIN_RATIO}_vecmap 



#################################


BLD=/home/bigdata/thomas/dataspace/IJCAI16_results
DS2=/home/bigdata/thomas/dataspace/graph/ppi/sub_graph/subgraph3/random_clone/del_edge,p=
LOGNAME=sub_vs_sub_del_edge
TRAIN_RATIO=0.2
RATIO=0.1

time python vecmap/map_embeddings.py --semi_supervised ${DS2}${RATIO},n=${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.train.dict \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource_vecmap.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget_vecmap.emb \
    --cuda

python vecmap/eval_translation.py \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource_vecmap.emb \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget_vecmap.emb \
-d ${DS2}${RATIO},n=${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.test.dict \
--retrieval nn --cuda > ${LOGNAME}_${TRAIN_RATIO}_vecmap_IJCAI


TRAIN_RATIO=0.03


time python vecmap/map_embeddings.py --semi_supervised ${DS2}${RATIO},n=${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.train.dict \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource_vecmap.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget_vecmap.emb \
    --cuda

python vecmap/eval_translation.py \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource_vecmap.emb \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget_vecmap.emb \
-d ${DS2}${RATIO},n=${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.test.dict \
--retrieval nn --cuda > ${LOGNAME}_${TRAIN_RATIO}_vecmap_IJCAI


BLD=/home/bigdata/thomas/dataspace/IJCAI16_results
DS2=/home/bigdata/thomas/dataspace/graph/ppi/sub_graph/subgraph3/random_clone/add_edge,p=
LOGNAME=sub_vs_sub_add_edge
TRAIN_RATIO=0.2
RATIO=0.1

time python vecmap/map_embeddings.py --semi_supervised ${DS2}${RATIO},n=${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.train.dict \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource_vecmap.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget_vecmap.emb \
    --cuda

python vecmap/eval_translation.py \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource_vecmap.emb \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget_vecmap.emb \
-d ${DS2}${RATIO},n=${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.test.dict \
--retrieval nn --cuda > ${LOGNAME}_${TRAIN_RATIO}_vecmap_IJCAI


TRAIN_RATIO=0.03


time python vecmap/map_embeddings.py --semi_supervised ${DS2}${RATIO},n=${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.train.dict \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource_vecmap.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget_vecmap.emb \
    --cuda

python vecmap/eval_translation.py \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource_vecmap.emb \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget_vecmap.emb \
-d ${DS2}${RATIO},n=${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.test.dict \
--retrieval nn --cuda > ${LOGNAME}_${TRAIN_RATIO}_vecmap_IJCAI





BLD=/home/bigdata/thomas/dataspace/IJCAI16_results
DS2=/home/bigdata/thomas/dataspace/graph/ppi/sub_graph/subgraph3/random_delete_node/del,p=
RATIO=0.1
LOGNAME=sub_vs_sub_del_nodes01

TRAIN_RATIO=0.2

time python vecmap/map_embeddings.py --semi_supervised ${DS2}${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.train.dict \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource_vecmap.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget_vecmap.emb \
    --cuda

python vecmap/eval_translation.py \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource_vecmap.emb \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget_vecmap.emb \
-d ${DS2}${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.test.dict \
--retrieval nn --cuda > ${LOGNAME}_${TRAIN_RATIO}_vecmap_IJCAI



TRAIN_RATIO=0.03

time python vecmap/map_embeddings.py --semi_supervised ${DS2}${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.train.dict \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource_vecmap.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget_vecmap.emb \
    --cuda

python vecmap/eval_translation.py \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource_vecmap.emb \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget_vecmap.emb \
-d ${DS2}${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.test.dict \
--retrieval nn --cuda > ${LOGNAME}_${TRAIN_RATIO}_vecmap_IJCAI



BLD=/home/bigdata/thomas/dataspace/IJCAI16_results
DS2=/home/bigdata/thomas/dataspace/graph/ppi/sub_graph/subgraph3/random_delete_node/del,p=
RATIO=0.2
LOGNAME=sub_vs_sub_del_nodes02

TRAIN_RATIO=0.2

time python vecmap/map_embeddings.py --semi_supervised ${DS2}${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.train.dict \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource_vecmap.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget_vecmap.emb \
    --cuda

python vecmap/eval_translation.py \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource_vecmap.emb \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget_vecmap.emb \
-d ${DS2}${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.test.dict \
--retrieval nn --cuda > ${LOGNAME}_${TRAIN_RATIO}_vecmap_IJCAI



TRAIN_RATIO=0.03

time python vecmap/map_embeddings.py --semi_supervised ${DS2}${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.train.dict \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource_vecmap.emb \
    ${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget_vecmap.emb \
    --cuda

python vecmap/eval_translation.py \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAIsource_vecmap.emb \
${BLD}/embeddings/${LOGNAME}_train_percent${TRAIN_RATIO}_IJCAItarget_vecmap.emb \
-d ${DS2}${RATIO}/dictionaries/node,split=${TRAIN_RATIO}.test.dict \
--retrieval nn --cuda > ${LOGNAME}_${TRAIN_RATIO}_vecmap_IJCAI




