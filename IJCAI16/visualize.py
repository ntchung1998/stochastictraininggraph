import argparse
import numpy as np
import os

def parse_args():
    parser = argparse.ArgumentParser(description="Convert .emb file to .tsv file for visualizing in http://projector.tensorflow.org/")
    parser.add_argument('--source_path', default=None, type=str)
    parser.add_argument('--target_path', default=None, type=str)
    parser.add_argument('--source_map_path', default=None, type=str)
    parser.add_argument('--prefix', default="", type=str)

    parser.add_argument('--out_dir', default="$HOME/dataspace/graph/pale_facebook/pale_embeddings", help="Output directory.")

    return parser.parse_args()

def load_embedding(path1, path2, path3):
    embeds1 = None
    embeds2 = None
    embeds3 = None
    if path1 is not None:
        embeds1 = np.load(path1)
    if path2 is not None:
        embeds2 = np.load(path2)
    if path3 is not None:
        embeds3 = np.load(path3)
    return embeds1, embeds2, embeds3

def convert_and_save(embeds1, embeds2, embeds3, out_dir, prefix):
    """
    embeds: numpy array, shape NxD which N is the number of nodes, D is the size of embedded vector.
    id_map: dict, keys are nodes id, values are indexes.
    out_dir: directory to save output files.
    """
    # if not os.path.exists(out_dir):
    #     os.makedirs(out_dir)
    if embeds1 is not None:
        print("source_tsv file has been save to: ", os.path.join(out_dir, prefix + '_source.tsv'))
        out_embeds1 = open(os.path.join(out_dir, prefix + '_source.tsv'), 'w+')
        for idx, embed in enumerate(embeds1):
            out_embeds1.write('\t'.join(map(str, embed)))
            out_embeds1.write('\n')
        out_embeds1.close()

    if embeds2 is not None:
        print("target_tsv file has been save to: ", os.path.join(out_dir, prefix + '_target.tsv'))
        out_embeds1 = open(os.path.join(out_dir, prefix + '_target.tsv'), 'w+')
        for idx, embed in enumerate(embeds2):
            out_embeds1.write('\t'.join(map(str, embed)))
            out_embeds1.write('\n')
        out_embeds1.close()

    if embeds3 is not None:
        print("source_with_map file has been save to: ", os.path.join(out_dir, prefix +'_source_with_map.tsv'))
        out_embeds1 = open(os.path.join(out_dir, prefix + '_source_with_map.tsv'), 'w+')
        for idx, embed in enumerate(embeds3):
            out_embeds1.write('\t'.join(map(str, embed)))
            out_embeds1.write('\n')
        out_embeds1.close()


if __name__ == '__main__':
    args = parse_args()
    embeds1, embeds2, embeds3 = load_embedding(args.source_path, args.target_path, args.source_map_path)
    convert_and_save(embeds1, embeds2, embeds3, args.out_dir, args.prefix)
