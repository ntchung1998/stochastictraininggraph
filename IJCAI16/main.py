from IJCAI16.embedding_models import PaleEmbedding
from IJCAI16.mapping_model import PaleMappingLinear, PaleMappingMlp
from IJCAI16.dataset import Dataset
from IJCAI16.utils import draw_result, translation, mapping_train_, train_embedding, to_word2vec_format, read_emb
import argparse
import os
import collections
import json
import time
import numpy as np
import os
from copy import deepcopy
import torch.nn as nn
import torch
import matplotlib.pyplot as plt
import torch.nn.functional as F
import random

from bayes_opt import BayesianOptimization



def parse_args():
    parser = argparse.ArgumentParser(description="Run supervised graphSAGE")
    parser.add_argument('--prefix1',             default="/home/bigdata/thomas/dataspace/graph/pale_facebook/random_clone/sourceclone,alpha_c=0.9,alpha_s=0.9/pale_facebook")
    parser.add_argument('--prefix2',             default="/home/bigdata/thomas/dataspace/graph/pale_facebook/random_clone/sourceclone,alpha_c=0.9,alpha_s=0.9/pale_facebook")
    parser.add_argument('--base_log_dir',        default="/home/bigdata/thomas/dataspace/IJCAI16_results")
    parser.add_argument('--cuda',                action='store_true')
    parser.add_argument('--log_name',            default='')

    parser.add_argument('--learning_rate1',      default=0.01,        type=float)
    parser.add_argument('--embedding_dim',       default=300,         type=int) 
    parser.add_argument('--batch_size_embedding',default=150,         type=int)
    parser.add_argument('--embedding_epochs',    default=50,        type=int)
    parser.add_argument('--neg_sample_size',     default=10,          type=int)
    parser.add_argument('--embedding_model',     default='pale')
    parser.add_argument('--neg_weight',          default=1.0,         type=float)
    parser.add_argument('--use_features',        action='store_true')
    parser.add_argument('--add_feature_before_map', action='store_true')

    parser.add_argument('--learning_rate2',      default=0.001,       type=float)
    parser.add_argument('--batch_size_mapping',  default=128,         type=int)
    parser.add_argument('--mapping_epochs',      default=300,         type=int)
    parser.add_argument('--mapping_model',       default='linear')
    parser.add_argument('--train_percent',       default=0.2,        type=float)
    parser.add_argument('--activate_function',   default='sigmoid')

    # FOR LOADING EMBEDDING
    parser.add_argument('--embedding_dir1'  ,    default=None)
    parser.add_argument('--embedding_dir2',      default=None)
    parser.add_argument('--max_iter',            default=1000000000, type=int)
    parser.add_argument('--train_dict',          default=None)    
    parser.add_argument('--val_dict',            default=None)


    return parser.parse_args()


def train(data, args):
    # args.log_name = "st_vs_st_with_feature" 
    args.log_name +=  '_IJCAI'
    ###################################################################################
    #                                       DATA
    ###################################################################################

    source_train_nodes = deepcopy(data.source_train_node_idx)
    source_val_nodes = deepcopy(data.source_val_node_idx)
    num_source_nodes = len(source_train_nodes) + len(source_val_nodes)
    target_train_nodes = deepcopy(data.target_train_node_idx)
    target_val_nodes = deepcopy(data.target_val_node_idx)
    num_target_nodes = len(target_train_nodes) + len(target_val_nodes)

    source_id2idx = data.source_id2idx
    target_id2idx = data.target_id2idx

    stdict = deepcopy(data.stdict)

    deg1 = deepcopy(data.source_deg) 
    deg2 = deepcopy(data.target_deg)
    source_edges = deepcopy(data.source_edges) 
    target_edges = deepcopy(data.target_edges)

    print("number of source edges: ", len(source_edges))
    print("number of target edges: ", len(target_edges))
    source_val_nodes = [node for node in source_val_nodes if node in list(stdict.keys())]
    ######################################################################################
    #                                   EMBEDDING MODEL
    ######################################################################################

    ############# Load embedding: if embedding_dir is not None 
    # if args.embedding_dir1 is not None:
    #     if not os.path.exists(args.embedding_dir1):
    #         raise Exception("haven't had embedding yet!!!")
    #     srcfile = open(args.embedding_dir1, encoding='utf-8', errors='surrogateescape')
    #     trgfile = open(args.embedding_dir2, encoding='utf-8', errors='surrogateescape')
    #     embeddings_trained1 = read_emb(srcfile, source_id2idx)
    #     embeddings_trained2 = read_emb(trgfile, target_id2idx)

    #     embeddings_trained1 = torch.FloatTensor(embeddings_trained1)
    #     embeddings_trained2 = torch.FloatTensor(embeddings_trained2)
    #     if args.cuda:
    #         embeddings_trained1 = embeddings_trained1.cuda()
    #         embeddings_trained2 = embeddings_trained2.cuda()

    #     embedding_dim = args.embedding_dim
        

    ############# Else: define embedding
    # else:
        # embedding1 for source
    

    print("learning_rate1: ", learning_rate1)
    print("learning_rate2: ", learning_rate2)
    print("neg_sample_size: ", int(neg_sample_size))
    print("mapping_epochs: ", int(mapping_epochs))

    embedding1 = PaleEmbedding(
                            n_nodes=num_source_nodes,
                            embedding_dim=args.embedding_dim,
                            deg=deg1, 
                            neg_sample_size=args.neg_sample_size,
                            cuda=args.cuda,
                            neg_weight=args.neg_weight,
                            )
    # embedding2 for target
    embedding2 = PaleEmbedding(
                            n_nodes=num_target_nodes,
                            embedding_dim=args.embedding_dim,
                            deg=deg2,
                            neg_sample_size=args.neg_sample_size,
                            cuda=args.cuda,
                            neg_weight=args.neg_weight
                            )
    
    embedding_dim = args.embedding_dim

    if args.cuda:
        embedding1 = embedding1.cuda()
        embedding2 = embedding2.cuda()
    optimizer_e1 = torch.optim.Adam(filter(lambda p : p.requires_grad, embedding1.parameters()),lr=args.learning_rate1)
    optimizer_e2 = torch.optim.Adam(filter(lambda p : p.requires_grad, embedding2.parameters()),lr=args.learning_rate1)

    print("Start training for source embedding")
    embeddings_trained1 = train_embedding(embedding1, source_edges, optimizer_e1, args)
    # to_word2vec_format(embeddings_trained1, data.source_node_ids, args.base_log_dir + "/embeddings/" \
        # + args.log_name + "source.emb", dim = len(embeddings_trained1[0]), id2idx=source_id2idx)


    print("Start training for target embedding")
    embeddings_trained2 = train_embedding(embedding2, target_edges, optimizer_e2, args)
    # to_word2vec_format(embeddings_trained2, data.target_node_ids, args.base_log_dir + "/embeddings/" \
        # + args.log_name + "target.emb", dim = len(embeddings_trained2[0]), id2idx=target_id2idx)

    def define_model(learning_rate2, mapping_epochs):
        # args.learning_rate1 = learning_rate1
        args.learning_rate2 = learning_rate2
        # args.neg_sample_size = int(neg_sample_size)
        args.mapping_epochs = int(mapping_epochs)


        #######################################################################################
        #                                   MAPPING MODEL
        #######################################################################################        
        if args.mapping_model == 'linear':
            print("Use linear mapping")
            mapping_model = PaleMappingLinear(
                                        embedding_dim=embedding_dim,
                                        source_embedding=embeddings_trained1,
                                        target_embedding=embeddings_trained2,
                                        )
        else:
            print("Use Mpl mapping")
            mapping_model = PaleMappingMlp(
                                        embedding_dim=embedding_dim,
                                        source_embedding=embeddings_trained1,
                                        target_embedding=embeddings_trained2,
                                        activate_function=args.activate_function
                                        )
        if args.cuda:
            mapping_model = mapping_model.cuda()

        optimizer_m = torch.optim.Adam(filter(lambda p : p.requires_grad, mapping_model.parameters()), lr=args.learning_rate2)

        print("Start training for mapping function")
        train_losses, val_losses = mapping_train_(mapping_model, source_train_nodes, source_val_nodes, stdict, optimizer_m, args, data)
        draw_result(train_losses, val_losses, None, args, 'sam_' + args.log_name)

        ########################################################################################
        #                                       EVALUATE
        ########################################################################################
        source_after_mapping = mapping_model(embeddings_trained1)
        # to_word2vec_format(source_after_mapping, data.source_node_ids, args.base_log_dir + "/embeddings/" + args.log_name + "source_after_mapping.emb", dim = len(embeddings_trained1[0]), id2idx=source_id2idx)

        train_acc, val_acc = translation(source_after_mapping, embeddings_trained2, source_train_nodes, stdict)

        print("Train_acc: ", train_acc, " Val_acc: ", val_acc)

        if not os.path.exists(args.base_log_dir + '/results'):
            os.makedirs(args.base_log_dir + '/results')

        with open(args.base_log_dir + '/results/' + args.log_name + '.txt', 'w') as f:
            f.write("Train_acc: %s, Val_acc: %s \n" %(train_acc, val_acc))

        print("result has been saved to: ", args.base_log_dir + '/results/' + args.log_name + '.txt')

        return val_acc

    # svcBO = BayesianOptimization(svccv,
    #     {'C': (0.001, 100), 'gamma': (0.0001, 0.1)})
    # svcBO.explore({'C': [0.001, 0.01, 0.1], 'gamma': [0.001, 0.01, 0.1]}, eager=True)
    # svcBO.maximize(n_iter=10, **gp_params)
    # print('-' * 53)
    # print('Final Results')
    # print('SVC: %f' % svcBO.res['max']['max_val'])
    gp_params = {"alpha": 1e-5}
    paleBO = BayesianOptimization(define_model,
        {#'learning_rate1':(0.001, 0.1), 
        'learning_rate2':(0.0005, 0.1), \
        #'neg_sample_size':(int(1), int(30)), 
        'mapping_epochs':(int(20), int(2000))})

    paleBO.explore({#'learning_rate1':[0.005, 0.01], 
        'learning_rate2':[0.005, 0.01], \
        #'neg_sample_size': [int(10), int(20)], \
        'mapping_epochs':[int(100), int(1000)]})

    paleBO.maximize(**gp_params)

    print('-'*53)
    print("Final Results")
    print("pale: %f" % paleBO.res['max']['max_val'])



if __name__ == "__main__":
    args = parse_args()
    print(args)
    seed = 123
    np.random.seed(seed)
    random.seed(seed)
    torch.manual_seed(seed)
    print(args.val_dict)
    data = Dataset(args.prefix1, args.prefix2, args.embedding_model, args.use_features, args.train_dict, args.val_dict)
    train(data, args)


