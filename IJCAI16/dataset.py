from __future__ import print_function
from pathlib import Path

import numpy as np
import random
import json
import sys
import os

import networkx as nx
from networkx.readwrite import json_graph
version_info = list(map(int, nx.__version__.split('.')))
major = version_info[0]
minor = version_info[1]
assert (major <= 1) and (minor <= 11), "networkx major version > 1.11"
import random
from copy import deepcopy
from sklearn.preprocessing import StandardScaler




def construct_deg(id2idx, node_ids, G):
    deg = np.zeros((len(id2idx),)).astype(int)
    for nodeid in node_ids:
        deg[id2idx[nodeid]] = len(G.neighbors(nodeid))
    return deg


def construct_edges(id2idx, G):
    edges1 = [(id2idx[n1], id2idx[n2]) for n1, n2 in G.edges()]
    edges2 = [(id2idx[n2], id2idx[n1]) for n1, n2 in G.edges()]
    
    edges = edges1 + edges2
    edges = np.array(edges)
    return edges

def construct_adj(max_degree, id2idx, G):
    if max_degree == None:
        raise Exception("Max degree hasn't been initialized")
    id_map = id2idx
    adj = -1*np.ones((len(id_map)+1, max_degree)).astype(int)
    
    #adj
    for nodeid in G.nodes():
        neighbors = np.array([id_map[neighbor]
            for neighbor in G.neighbors(nodeid)
        ])
        
        if len(neighbors) > max_degree:
            neighbors = np.random.choice(neighbors, max_degree, replace=False)
        elif len(neighbors) < max_degree:
            neighbors = np.random.choice(neighbors, max_degree, replace=True)
        
        adj[id_map[nodeid], :] = neighbors
    return adj


class Dataset():
    def __init__(self, prefix1, prefix2, model = 'pale', \
             use_features=False, train_dict_path= None, val_dict_path=None, max_degree=25):
        """
        Parameters
        ----------
        prefix1: str
            Link to the dataset where store prefix1-G.json and prefix1-id_map.json
        prefix2: str
            Link to the dataset where store prefix2-G.json and prefix2-id_map.json
        model: str, optional
            Type of embedding model used here, 'pale' or 'graphsage_mean'
        """

        self.prefix1 = prefix1
        self.prefix2 = prefix2
        self.model = model


        self.source_node_ids = None 
        self.target_node_ids = None 
        self.source_val_node_ids = None
        self.target_val_node_ids = None
        self.G1 = None
        self.G2 = None
        self.source_id2idx = None
        self.target_id2idx = None

        self.source_node_idx = None
        self.target_node_idx = None
        self.source_train_node_idx = None 
        self.target_train_node_idx = None
        self.source_val_node_idx = None
        self.target_val_node_idx = None
        
        self.stdict = None 

        self.source_edges = None 
        self.target_edges = None

        self.source_deg = None
        self.target_deg = None

        self.source_features = None 
        self.target_features = None 

        self.use_features = use_features
        self.source_train_node_ids = None 
        self.target_train_node_ids = None
        self.source_common_node_ids = None
        self.target_common_node_ids = None

        self.train_dict_path = train_dict_path
        self.val_dict_path = val_dict_path

        self.max_degree = max_degree

        self.train_dict = {}
        self.val_dict = {}
        with open(train_dict_path) as fp:
            for line in fp:
                pair = line.split()
                self.train_dict[pair[0]] = pair[1]


        with open(val_dict_path) as fp:
            for line in fp:
                pair = line.split()
                self.val_dict[pair[0]] = pair[1]


        self.train_dict_inverse = {k:v for v, k in self.train_dict.items()}
        self.val_dict_inverse = {k:v for v, k in self.val_dict.items()}
        self.get_ids_data()
        self.extend_data()
        self.get_idx_data()


    def read_data(self, prefix):
        print("-----------------------------------------------")
        ## Load graph data
        print("Loading graph data from {0}".format(prefix + "-G.json"))
        G_data = json.load(open(prefix + "-G.json"))
        G = json_graph.node_link_graph(G_data)

        if isinstance(G.nodes()[0], int):
            conversion = lambda n : int(n)
        else:
            conversion = lambda n : n


        for node in G.nodes():
            if not 'val' in G.node[node] or not 'test' in G.node[node]:
                G.remove_node(node)
                broken_count += 1

        print("File loaded successfully")
        id2idx = json.load(open(prefix + "-id_map.json"))
        id2idx = {conversion(k):int(v) for k,v in id2idx.items()}

        features = None
        if self.use_features:
            assert os.path.exists(prefix + "-feats.npy"), "features file doesn't exist"
            features = np.load(prefix + "-feats.npy")
        print("-----------------------------------------------")

        return G, id2idx, features


    def get_ids_data(self):
        self.G1, self.source_id2idx, self.source_features = self.read_data(self.prefix1)
        self.G2, self.target_id2idx, self.target_features = self.read_data(self.prefix2)

        self.source_node_ids = np.array(self.G1.nodes())
        self.target_node_ids = np.array(self.G2.nodes())

        self.source_train_node_ids = np.array(list(self.train_dict.keys()))
        self.target_train_node_ids = np.array(list(self.train_dict.values()))

        self.source_common_node_ids = np.array(list(self.train_dict.keys()))
        self.target_common_node_ids = np.array(list(self.train_dict.values()))

        self.source_val_node_ids = np.array(list(self.val_dict.keys()))
        self.target_val_node_ids = np.array(list(self.val_dict.values()))


    def extend_data(self, fast=False):
        print("Start extending data...")
        for node in self.source_train_node_ids:
            n1 = [ele for ele in self.G1.neighbors(node) if ele in self.source_common_node_ids] 
            n2 = [ele for ele in self.G2.neighbors(self.train_dict[node]) if ele in self.target_common_node_ids]
            n1_to_n2 = [self.train_dict[n] for n in n1] 
            n2_to_n1 = [self.train_dict_inverse[n] for n in n2]
            nodes_to_extend_for_source = n2_to_n1
            nodes_to_extend_for_target = n1_to_n2
            for n in nodes_to_extend_for_source:
                self.G1.add_edge(node, n)
            for n in nodes_to_extend_for_target:
                self.G2.add_edge(self.train_dict[node], n)
        print("done extending")


    def get_idx_data(self):
        self.source_node_idx = np.array([self.source_id2idx[nodeid] for nodeid in self.source_node_ids])
        self.target_node_idx = np.array([self.target_id2idx[nodeid] for nodeid in self.target_node_ids])

        self.source_train_node_idx = np.array([self.source_id2idx[nodeid] for nodeid in self.source_train_node_ids])
        self.target_train_node_idx = np.array([self.target_id2idx[nodeid] for nodeid in self.target_train_node_ids])

        self.source_val_node_idx = np.array([nodeidx for nodeidx in self.source_node_idx if nodeidx not in self.source_train_node_idx])
        self.target_val_node_idx = np.array([nodeidx for nodeidx in self.target_node_idx if nodeidx not in self.target_train_node_idx])

        self.stdict = {}

        for s_nodeid in self.source_train_node_ids:
            t_nodeid = self.train_dict[s_nodeid]
            self.stdict[self.source_id2idx[s_nodeid]] = self.target_id2idx[t_nodeid]

        for s_nodeid in self.source_val_node_ids:
            t_nodeid = self.val_dict[s_nodeid]
            self.stdict[self.source_id2idx[s_nodeid]] = self.target_id2idx[t_nodeid]


        self.source_deg = construct_deg(self.source_id2idx, self.source_node_ids, self.G1)
        self.target_deg = construct_deg(self.target_id2idx, self.target_node_ids, self.G2)

        self.source_edges = construct_edges(self.source_id2idx, self.G1)
        self.target_edges = construct_edges(self.target_id2idx, self.G2)


        if self.model != 'pale':
            self.source_adj = construct_adj(self.max_degree, self.source_id2idx, self.G1)
            self.target_adj = construct_adj(self.max_degree, self.target_id2idx, self.G2)