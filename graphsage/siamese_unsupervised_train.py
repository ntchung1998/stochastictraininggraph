import os
import time
import numpy as np

# import sklearn
# from sklearn import metrics
import argparse
import torch.nn as nn
import torch
# from torch.autograd import Variable
# from sklearn.metrics import f1_score

from graphsage.unsupervised_models import UnsupervisedGraphSage
from graphsage.siamese_unsupervised_model import SiameseUnsupervisedGraphSage
from graphsage.neigh_samplers import UniformNeighborSampler
import torch.nn.functional as F
from graphsage.dataset import Dataset
from graphsage.aggregators import MeanAggregator, MeanPoolAggregator, MaxPoolAggregator, LSTMAggregator
import subprocess
import re
import json

# Use in evaluate
from sklearn.linear_model import SGDClassifier
from sklearn.dummy import DummyClassifier
from sklearn.metrics import f1_score
from sklearn.multioutput import MultiOutputClassifier

from tensorboardX import SummaryWriter
summary_writer = SummaryWriter()

def parse_args():
    parser = argparse.ArgumentParser(description="Run supervised graphSAGE")
    parser.add_argument('--prefix_source',    default="example_data/ppi/graphsage/ppi",  help="Source data directory with prefix")
    parser.add_argument('--prefix_target',    default="example_data/ppi/graphsage/ppi",  help="Target data directory with prefix")
    parser.add_argument('--cuda',             default=False,           help="Run on cuda or not", type=bool)
    parser.add_argument('--model',            default='graphsage_mean',help='Model names. See README for possible values.')
    parser.add_argument('--multiclass',       default=False,           help='Whether use 1-hot labels or indices.', type=bool)
    parser.add_argument('--learning_rate',    default=0.01,            help='Initial learning rate.', type=float)
    parser.add_argument('--concat',           default=True,            help='whether to concat', type=bool)
    parser.add_argument('--epochs',           default=10,              help='Number of epochs to train.', type=int)
    parser.add_argument('--dropout',          default=0.0,             help='Dropout rate (1 - keep probability).', type=float)
    parser.add_argument('--weight_decay',     default=0.0,             help='Weight for l2 loss on embedding matrix.', type=float)
    parser.add_argument('--max_degree',       default=25,              help='Maximum node degree.', type=int)
    parser.add_argument('--samples_1',        default=10,              help='Number of samples in layer 1', type=int)
    parser.add_argument('--samples_2',        default=25,              help='Number of samples in layer 2', type=int)
    parser.add_argument('--samples_3',        default=0,               help='Number of users samples in layer 3. (Only for mean model)', type=int)
    parser.add_argument('--dim_1',            default=128,             help='Size of output dim (final is 2x this, if using concat)', type=int)
    parser.add_argument('--dim_2',            default=128,             help='Size of output dim (final is 2x this, if using concat)', type=int)
    parser.add_argument('--random_context',   default=False,           help='Whether to use random context or direct edges', type=bool)
    parser.add_argument('--batch_size',       default=256,             help='Minibatch size.', type=int)
    parser.add_argument('--base_log_dir',     default='visualize/ppi/', help='Base directory for logging and saving embeddings')
    parser.add_argument('--print_every',      default=10,              help="How often to print training info.", type=int)
    parser.add_argument('--max_total_steps',  default=10**10,          help="Maximum total number of iterations", type=int)
    parser.add_argument('--validate_iter',    default=5000,            help="How often to run a validation minibatch.", type=int)
    parser.add_argument('--validate_batch_size', default=256,          help="How many nodes per validation sample.", type=int)
    parser.add_argument('--neg_sample_size',  default=20,              help='Negative sample size', type=int)
    parser.add_argument('--identity_dim',     default=0,               help='Set to positive value to use node_embedding_prep. Default 0.', type=int)
    parser.add_argument('--save_embeddings',  default=False,           help="Whether to save embeddings.", type=bool)
    parser.add_argument('--load_embedding_samples_dir',  default=None, help="Whether to load embedding samples.")
    parser.add_argument('--save_embedding_samples',  default=False,    help="Whether to save embedding samples", type=bool)
    parser.add_argument('--seed',             default=123,             help="Random seed", type=int)
    parser.add_argument('--load_adj_dir',     default=None,            help="Adj dir load")
    parser.add_argument('--save_model',       default=False,           help="Save model's parameters", type=bool)
    parser.add_argument('--load_model_dir',   default=None,            help="Load pretrain model")
    parser.add_argument('--no_feature',       default=False,           help='whether to use features')
    parser.add_argument('--train_dict_dir',   default='example_data/ppi/dictionaries/node,split=0.2.train.dict', help='train dictionary directory')
    parser.add_argument('--val_dict_dir',     default='example_data/ppi/dictionaries/node,split=0.2.test.dict', help='val dictionary directory')
    parser.add_argument('--groundtruth',      default='example_data/ppi/dictionaries/node,split=0.2.all.dict', help='groundtruth')
    parser.add_argument('--embedding_loss_weight',     default=1,      help='Embedding loss weight', type=int)
    parser.add_argument('--mapping_loss_weight',       default=1,      help='Mapping loss weight', type=int)
    parser.add_argument('--use_random_walks',  default=False,          help="Whether to use random walk.", type=bool)
    parser.add_argument('--load_walks',        default=False,          help="Whether to load walk file.", type=bool)
    parser.add_argument('--num_walk',         default=50,              help="Number of walk from each node.", type=int)
    parser.add_argument('--walk_len',         default=5,               help="Length of each walk.", type=int)
    parser.add_argument('--normalize',        default=True,           help="Normalize at unsup model", type=bool)
    parser.add_argument('--map_fc', default="linear", help="Type of mapping function")
    return parser.parse_args()

def load_data(args, supervised=False, max_degree=25, load_adj_dir = None, use_random_walks = True, load_walks=True, num_walk=50, walk_len=5, train_all_edge = True):
    source_dataset = Dataset()
    source_dataset.load_data(prefix = args.prefix_source, normalize=True, supervised=supervised, max_degree=max_degree, train_all_edge=train_all_edge, use_random_walks = use_random_walks, load_walks=load_walks, num_walk=num_walk, walk_len=walk_len)
    target_dataset = Dataset()
    target_dataset.load_data(prefix = args.prefix_target, normalize=True, supervised=supervised, max_degree=max_degree, train_all_edge=train_all_edge, use_random_walks = use_random_walks, load_walks=load_walks, num_walk=num_walk, walk_len=walk_len)
    return source_dataset, target_dataset

def loadDict(source_dataset, target_dataset, dict_dir):
    source_dictionary = np.zeros(source_dataset.num_total_node).astype(int)
    target_dictionary = np.zeros(target_dataset.num_total_node).astype(int)
    source_dictionary = source_dictionary - 1 #Initialize dictionary as an array with all elements = -1
    target_dictionary = target_dictionary - 1

    with open(dict_dir) as fp:
        for line in fp:
            pair = line.split()
            source = source_dataset.id_map[source_dataset.conversion(pair[0])]
            target = target_dataset.id_map[target_dataset.conversion(pair[1])]
            source_dictionary[source] = target
            target_dictionary[target] = source

    return source_dictionary, target_dictionary

def run_regression(train_embeds, train_labels, test_embeds, test_labels, name="embedding"):
    print("Running regression...")
    dummy = MultiOutputClassifier(DummyClassifier())
    dummy.fit(train_embeds, train_labels)
    log = MultiOutputClassifier(SGDClassifier(loss="log", max_iter=5, tol=None), n_jobs=10)
    log.fit(train_embeds, train_labels)

    log_predict = log.predict(test_embeds)
    f1_sc = f1_score(test_labels, log_predict, average='micro')
    print("F1 score", name, " :", f1_sc)
    return f1_sc

def evaluate_features(dataset, name):
    train_labels = np.array([dataset.class_map[id] for id in dataset.train_nodes_ids])
    test_labels = np.array([dataset.class_map[id] for id in dataset.test_nodes_ids])
    train_features = dataset.feats[[dataset.id_map[id] for id in dataset.train_nodes_ids]]
    test_features = dataset.feats[[dataset.id_map[id] for id in dataset.test_nodes_ids]]
    run_regression(train_features, train_labels, test_features, test_labels, name)

def to_word2vec_format(embeddings, dataset, node_ids, output_file_name, dim, pref=""):
    with open(output_file_name, 'w') as f_out:
        f_out.write("%s %s\n"%(len(node_ids), dim))
        for i, node in enumerate(node_ids):
            idx = dataset.id_map[node]
            txt_vector = ["%s" % embeddings[idx][j] for j in range(dim)]
            f_out.write("%s%s %s\n" % (pref, node, " ".join(txt_vector)))
        f_out.close()

def log_dir(args):
    log_dir = args.base_log_dir + "/unsup"
    log_dir += "/{model:s}_{lr:0.2f}/".format(
            model=args.model,
            lr=args.learning_rate)
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)
    return log_dir

def save_embedding(model, dataset, all_nodes, nodes_ids, mode, summary=False):
    all_nodes = torch.LongTensor(all_nodes)
    if args.cuda:
        all_nodes = all_nodes.cuda()
    embeddings = [] # embed of all nodes
    model.set_mode(mode)

    iter_num = 0
    while True:
        batch_nodes = all_nodes[iter_num*args.validate_batch_size:(iter_num+1)*args.validate_batch_size]
        if batch_nodes.shape[0] == 0: break
        samples = model.unsupervised_model.sample(batch_nodes)
        outputs = model.forward_once(samples)
        if mode == "source":
            outputs = model.map_fc(outputs)
        for i in range(batch_nodes.shape[0]):
            embeddings.append(outputs[i,:].cpu().detach().numpy())
        iter_num += 1
    embeddings = np.vstack(embeddings)
    # train_labels = np.array([dataset.class_map[id] for id in dataset.train_nodes_ids])
    # test_labels = np.array([dataset.class_map[id] for id in dataset.test_nodes_ids])
    # train_embeds = embeddings[[dataset.id_map[id] for id in dataset.train_nodes_ids]]
    # test_embeds = embeddings[[dataset.id_map[id] for id in dataset.test_nodes_ids]]
    # f1_sc = run_regression(train_embeds, train_labels, test_embeds, test_labels)
    # print("F1_score {0}".format(f1_sc))
    if summary:
        summary_writer.add_embedding(embeddings, tag=mode)

    output_file_name = log_dir(args) + mode + ".emb"
    to_word2vec_format(embeddings, dataset, nodes_ids, output_file_name, dim = len(embeddings[0]))
    return output_file_name

def compute_accuracy(source_emb, target_emb, val_dict_dir):
    command = "python vecmap/eval_translation.py {} {} -d {} --retrieval nn".format(source_emb, target_emb, val_dict_dir)
    process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
    res, err = process.communicate()
    res = str(res)
    result = res.split("\\n")[-2]
    result = re.findall(r"[0-9\.]+", result)
    coverage = float(result[0])
    acc = float(result[1])
    return coverage, acc

def train_(model, source_dataset, target_dataset, source_edges, target_edges, optimizer, args):
    epochs, batch_size, cuda = args.epochs, args.batch_size, args.cuda

    avg_time = 0.0
    source_n_iters = len(source_edges)//batch_size
    #len(train_edges)%batch_size for case len%batch_size = 0
    if(len(source_edges) % batch_size > 0):
        source_n_iters += 1

    target_n_iters = len(target_edges)//batch_size
    #len(train_edges)%batch_size for case len%batch_size = 0
    if(len(target_edges) % batch_size > 0):
        target_n_iters += 1

    history = {
        'accs': [],
        'losses': [],
        'emb_losses': [],
        'mapping_losses': []
    }
    best_acc = -1

    total_steps = 0

    current_average_loss = 0

    # evaluate_features(source_dataset, name="source_features")
    # evaluate_features(target_dataset, name="target_features")

    for epoch in range(epochs):
        print("Epoch {0}".format(epoch))
        np.random.shuffle(source_edges)
        np.random.shuffle(target_edges)

        n_iters = source_n_iters if source_n_iters < target_n_iters else target_n_iters

        for iter in range(n_iters):
            batch_edges = torch.LongTensor(source_edges[iter*batch_size:(iter+1)*batch_size])
            if cuda:
                batch_edges = batch_edges.cuda()

            t = time.time()
            optimizer.zero_grad()

            loss, embedding_loss, mapping_loss = model.loss(batch_edges[:,0], batch_edges[:,1], mode="source")
            loss.backward()
            nn.utils.clip_grad_value_(model.parameters(), 5.0)
            optimizer.step()
            avg_time = (avg_time * total_steps + time.time() - t) / (total_steps + 1)

            if total_steps % args.print_every == 0:
                    print("Iter:", '%03d' %iter,
                        "train_loss=", "{:.5f}".format(loss.item()),
                        "embedding_loss=", "{:.5f}".format(embedding_loss.item()),
                        "mapping_loss=", "{:.5f}".format(mapping_loss.item()),
                        "time", "{:.5f}".format(avg_time)
                        )

            current_average_loss += loss.item()

            # Add summary
            summary_writer.add_scalar('data/loss', loss, iter)
            summary_writer.add_scalar('data/embedding_loss', embedding_loss, iter)
            summary_writer.add_scalar('data/mapping_loss', mapping_loss, iter)

            # print("Forward target")
            batch_edges = torch.LongTensor(target_edges[iter*batch_size:(iter+1)*batch_size])
            if cuda:
                batch_edges = batch_edges.cuda()

            t = time.time()
            optimizer.zero_grad()

            loss, embedding_loss, mapping_loss = model.loss(batch_edges[:,0], batch_edges[:,1], mode="target")
            loss.backward()
            nn.utils.clip_grad_value_(model.parameters(), 5.0)
            optimizer.step()

            if total_steps % args.print_every == 0:
                    print("Iter:", '%03d' %iter,
                        "train_loss=", "{:.5f}".format(loss.item()),
                        "embedding_loss=", "{:.5f}".format(embedding_loss.item()),
                        "mapping_loss=", "{:.5f}".format(mapping_loss.item()),
                        "time", "{:.5f}".format(avg_time)
                        )

            summary_writer.add_scalar('data/loss', loss, iter)
            summary_writer.add_scalar('data/embedding_loss', embedding_loss, iter)
            summary_writer.add_scalar('data/mapping_loss', mapping_loss, iter)

            if total_steps % args.validate_iter == 0:
                # save embeds
                source_emb = save_embedding(model, source_dataset, source_dataset.nodes, source_dataset.nodes_ids, mode="source")
                target_emb = save_embedding(model, target_dataset, target_dataset.nodes, target_dataset.nodes_ids, mode="target")
                # compute accuracy
                train_coverage, train_acc = compute_accuracy(source_emb, target_emb, args.train_dict_dir)
                coverage, acc = compute_accuracy(source_emb, target_emb, args.val_dict_dir)
                history['accs'].append(acc)
                history['losses'].append(loss.item())
                history['emb_losses'].append(embedding_loss.item())
                history['mapping_losses'].append(mapping_loss.item())
                print ("Train accuracy: {}% and coverage: {}".format(train_acc, train_coverage))
                print ("Accuracy: {}% and coverage: {}".format(acc, coverage))

                summary_writer.add_scalar('data/acc', acc, iter)

                if acc > best_acc:
                    best_acc = acc
                    print ("Best accuracy: {}% and coverage: {}".format(acc, coverage))
                    with open(log_dir(args) + 'model_info.txt', 'w') as fp:
                        fp.write(str(model))
                    torch.save(model, log_dir(args) + 'model.pt')

            total_steps += 1
            if total_steps > args.max_total_steps:
                break

    source_emb = save_embedding(model, source_dataset, source_dataset.nodes, source_dataset.nodes_ids, mode="source")
    target_emb = save_embedding(model, target_dataset, target_dataset.nodes, target_dataset.nodes_ids, mode="target")
    # compute accuracy
    coverage, acc = compute_accuracy(source_emb, target_emb, args.val_dict_dir)
    history['accs'].append(acc)
    history['losses'].append(loss.item())
    history['emb_losses'].append(embedding_loss.item())
    history['mapping_losses'].append(mapping_loss.item())

    summary_writer.add_scalar('data/acc', acc, iter)
    if acc > best_acc:
        best_acc = acc
        print ("Best accuracy: {}% and coverage: {}".format(acc, coverage))
        with open(log_dir(args) + 'model_info.txt', 'w') as fp:
            fp.write(str(model))
        torch.save(model, log_dir(args) + 'model.pt')

    with open(log_dir(args) + 'history.json', 'w') as fp:
        fp.write(json.dumps(history))

    print("Best accuracy: {}%".format(best_acc))
    #Generate best embedding after run
    # import pdb
    # pdb.set_trace()
    best_model = torch.load(log_dir(args) + 'model.pt')
    source_emb = save_embedding(best_model, source_dataset, source_dataset.nodes, source_dataset.nodes_ids, mode="source", summary=True)
    target_emb = save_embedding(best_model, target_dataset, target_dataset.nodes, target_dataset.nodes_ids, mode="target", summary=True)
    coverage, acc = compute_accuracy(source_emb, target_emb, args.val_dict_dir)
    print("Best accuracy {0} and coverage {1}".format(acc, coverage))
    return avg_time

def train(source_dataset, target_dataset, args):
    if args.no_feature:
        source_dataset.feats = None
        target_dataset.feats = None

    if source_dataset.feats is None:
        assert args.identity_dim > 0, "if feats is None, requires identity_dim > 0"
        feat_dims = 0
        source_features = None
    else:
        feat_dims = source_dataset.feats.shape[1]
        source_features = torch.FloatTensor(source_dataset.feats)
        if(args.cuda):
            source_features = source_features.cuda()

    if target_dataset.feats is None:
        assert args.identity_dim > 0, "if feats is None, requires identity_dim > 0"
        feat_dims = 0
        target_features = None
    else:
        feat_dims = source_dataset.feats.shape[1]
        target_features = torch.FloatTensor(target_dataset.feats)
        if(args.cuda):
            target_features = target_features.cuda()

    if (source_features is None and target_features is not None):
        raise Exception("Source dataset doesn't have feature while target dataset do")
    elif (source_features is not None and target_features is None):
        raise Exception("Source dataset have feature while target dataset doesn't")
    elif (source_features is not None and target_features is not None):
        if (source_features.shape[1] != target_features.shape[1]):
            raise Exception("Source dataset have feature while target dataset doesn't")

    if args.identity_dim != 0:
        feat_dims = feat_dims + args.identity_dim

    aggregator_cls = None
    if args.model == "graphsage_mean":
        aggregator_cls = MeanAggregator
    elif args.model == "graphsage_meanpool":
        aggregator_cls = MeanPoolAggregator
    elif args.model == "graphsage_maxpool":
        aggregator_cls = MaxPoolAggregator
    elif args.model == "graphsage_lstm":
        aggregator_cls = LSTMAggregator
    else:
        raise Exception("Unknown aggregator: ", args.model)

    if args.samples_3 != 0:
        print("Using 3 aggregator layers")
        agg1 = aggregator_cls(input_dim=feat_dims, output_dim=args.dim_1, activation=F.relu, concat=args.concat, dropout=args.dropout)
        agg2 = aggregator_cls(input_dim=agg1.output_dim, output_dim=args.dim_2, activation=F.relu, concat=args.concat, dropout=args.dropout)
        agg3 = aggregator_cls(input_dim=agg2.output_dim, output_dim=args.dim_3, activation=False, concat=args.concat, dropout=args.dropout)
        agg_layers = [agg1, agg2, agg3]
        n_samples = [args.samples_1, args.samples_2, args.samples_3]
    elif args.samples_2 != 0:
        print("Using 2 aggregator layers")
        agg1 = aggregator_cls(input_dim=feat_dims, output_dim=args.dim_1, activation=F.relu, concat=args.concat, dropout=args.dropout)
        agg2 = aggregator_cls(input_dim=agg1.output_dim, output_dim=args.dim_2, activation=False, concat=args.concat, dropout=args.dropout)
        agg_layers = [agg1, agg2]
        n_samples = [args.samples_1, args.samples_2]
    else:
        print("Using 1 aggregator layers")
        agg_layers = [aggregator_cls(input_dim=feat_dims, output_dim=args.dim_1, activation=False, concat=args.concat, dropout=args.dropout)]
        n_samples = [args.samples_1]

    # Transform adj from numpy array to torch tensor
    source_adj = torch.LongTensor(source_dataset.adj)
    target_adj = torch.LongTensor(target_dataset.adj)

    if args.cuda:
        source_adj = source_adj.cuda()
        target_adj = target_adj.cuda()

    source_edges = source_dataset.train_edges
    target_edges = target_dataset.train_edges

    graphsage = UnsupervisedGraphSage(
                                features=source_features,
                                train_adj=source_adj,
                                adj=source_adj,
                                train_deg=source_dataset.deg,
                                deg=source_dataset.deg,
                                agg_layers=agg_layers,
                                n_samples=n_samples,
                                sampler=UniformNeighborSampler(source_adj),
                                fc=False,
                                identity_dim=args.identity_dim,
                                neg_sample_size=args.neg_sample_size)
    if args.cuda:
        graphsage = graphsage.cuda()

    source_dict, target_dict = torch.LongTensor(loadDict(source_dataset, target_dataset, args.train_dict_dir))
    if args.cuda:
        source_dict = source_dict.cuda()
        target_dict = target_dict.cuda()

    groundtruth, _ = torch.LongTensor(loadDict(source_dataset, target_dataset, args.groundtruth))
    if args.cuda:
        groundtruth = groundtruth.cuda()

    model = SiameseUnsupervisedGraphSage(graphsage, source_dict, target_dict, groundtruth,
        source_features, target_features,
        source_adj, target_adj,
        source_dataset.deg, target_dataset.deg,
        map_fc=args.map_fc, embedding_loss_weight=args.embedding_loss_weight, mapping_loss_weight=args.mapping_loss_weight)
    if args.cuda:
        model = model.cuda()

    optimizer = torch.optim.Adam(filter(lambda p : p.requires_grad, model.parameters()), lr=args.learning_rate, weight_decay=args.weight_decay)
    average_time = train_(model, source_dataset, target_dataset, source_edges, target_edges, optimizer, args)

    # summary_writer.export_scalars_to_json("./all_scalars.json")
    summary_writer.close()

    return average_time

if __name__ == "__main__":
    args = parse_args()
    print(args)

    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    source_dataset, target_dataset = load_data(args, supervised=False, max_degree=args.max_degree, use_random_walks=args.use_random_walks, load_walks=args.load_walks, num_walk=args.num_walk, walk_len=args.walk_len)
    train(source_dataset, target_dataset, args)
