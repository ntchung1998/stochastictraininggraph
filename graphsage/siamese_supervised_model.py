import torch
import torch.nn as nn
import time
import os
import numpy as np

import torch.nn.functional as F
from graphsage.neigh_samplers import UniformNeighborSampler
from graphsage.models import SampleAndAggregate

class SiameseSupervisedGraphSage(nn.Module):

    def __init__(self, supervised_model, dictionary, target_adj, embedding_loss_weight = 1, mapping_loss_weight = 1):

        """
        supervised_model: supervised model graphsage
        groundtruth: groundtruth dict
        target_adj: adj of the target set
        """

        super(SiameseSupervisedGraphSage, self).__init__()
        self.supervised_model = supervised_model
        self.embedding_dim = self.supervised_model.agg_layers[-1].output_dim
        self.map_fc=nn.Linear(self.embedding_dim, self.embedding_dim, bias=True)
        self.dictionary = dictionary
        self.target_adj = target_adj
        self.target_sampler = UniformNeighborSampler(self.target_adj)
        self.embedding_loss_weight = embedding_loss_weight
        self.mapping_loss_weight = mapping_loss_weight
        self.source_sample_fn = UniformNeighborSampler(self.supervised_model.adj)
        self.target_sample_fn = UniformNeighborSampler(target_adj)

    def loss(self, nodes, labels):

        # Embedding loss
        out, source_emb, target_emb = self.forward(nodes)
        embedding_loss = self.supervised_model.loss_fn(out, labels.squeeze())
        # Mapping loss
        diff = target_emb - self.map_fc(source_emb)
        # Mean of Frobenius norm on all diff vectors
        mapping_loss = diff.norm(dim=1).mean()
        return self.embedding_loss_weight * embedding_loss + self.mapping_loss_weight * mapping_loss
        # return embedding_loss

    def forward_once(self, samples):
        out = self.supervised_model.aggregate(samples)
        out = F.normalize(out, dim=1)
        return out

    def forward(self, nodes, mode='train'):

        """
        nodes: LongTensor
        """
        self.supervised_model.sample_fn = self.source_sample_fn
        source_samples = self.supervised_model.sample(nodes)
        source_emb = self.forward_once(source_samples)

        out = self.supervised_model.fc(source_emb)
        if self.supervised_model.multiclass:
            out = F.sigmoid(out)

        target_nodes = self.dictionary[nodes]
        anchor_idxs = np.argwhere(target_nodes.cpu().numpy() >= 0).reshape(-1)
        target_nodes = target_nodes[anchor_idxs]
        source_emb = source_emb[anchor_idxs]
        self.supervised_model.sample_fn = self.target_sample_fn
        target_samples = self.supervised_model.sample(target_nodes)
        target_emb = self.forward_once(target_samples)

        return out, source_emb, target_emb
