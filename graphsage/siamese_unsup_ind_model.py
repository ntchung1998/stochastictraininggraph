import torch
import torch.nn as nn
import time
import os
import numpy as np

import torch.nn.functional as F
from graphsage.neigh_samplers import UniformNeighborSampler
from graphsage.models import SampleAndAggregate
from graphsage.preps import prep_lookup
import pdb

class SiameseUnsupervisedGraphSage(nn.Module):

    def __init__(self, unsupervised_model,
        source_dict, target_dict,
        source_features, target_features,
        source_adj, target_adj,
        source_deg, target_deg):

        super(SiameseUnsupervisedGraphSage, self).__init__()
        self.unsupervised_model = unsupervised_model
        self.embedding_dim = self.unsupervised_model.agg_layers[-1].output_dim
        self.source_dict = source_dict
        self.target_dict = target_dict
        self.source_features = source_features
        self.target_features = target_features
        self.source_adj = source_adj
        self.target_adj = target_adj
        self.source_deg = source_deg
        self.target_deg = target_deg
        self.source_sample_fn = UniformNeighborSampler(source_adj)
        self.target_sample_fn = UniformNeighborSampler(target_adj)

        self.source_prep = self.unsupervised_model.prep

        if(type(self.source_prep) == prep_lookup['use_original_features']):
            self.target_prep = prep_lookup['use_original_features'](input_dim=self.source_prep.input_dim, n_nodes=self.target_deg.shape[0])
        elif (type(self.source_prep) == prep_lookup['use_identity_features']):
            self.target_prep = prep_lookup['use_identity_features'](input_dim=self.source_prep.input_dim, n_nodes=self.target_deg.shape[0],
                identity_dim=self.source_prep.identity_dim)

    def set_mode(self, mode):
        self.prev_mode = [
            self.unsupervised_model.sample_fn,
            self.unsupervised_model.features,
            self.unsupervised_model.train_adj,
            self.unsupervised_model.adj,
            self.unsupervised_model.train_deg,
            self.unsupervised_model.deg,
            self.unsupervised_model.prep
        ]
        if mode == "source":
            self.unsupervised_model.sample_fn = self.source_sample_fn
            self.unsupervised_model.features = self.source_features
            self.unsupervised_model.train_adj = self.source_adj
            self.unsupervised_model.adj = self.source_adj
            self.unsupervised_model.train_deg = self.source_deg
            self.unsupervised_model.deg = self.source_deg
            self.unsupervised_model.prep = self.source_prep

        elif mode == "target":
            self.unsupervised_model.sample_fn = self.target_sample_fn
            self.unsupervised_model.features = self.target_features
            self.unsupervised_model.train_adj = self.target_adj
            self.unsupervised_model.adj = self.target_adj
            self.unsupervised_model.train_deg = self.target_deg
            self.unsupervised_model.deg = self.target_deg
            self.unsupervised_model.prep = self.target_prep
        else:
            raise Exception("Unsupported mode {0}".format(mode))

    def reverse_mode(self):
        self.unsupervised_model.sample_fn,\
        self.unsupervised_model.features,\
        self.unsupervised_model.train_adj,\
        self.unsupervised_model.adj,\
        self.unsupervised_model.train_deg,\
        self.unsupervised_model.deg,\
        self.unsupervised_model.prep = self.prev_mode


    def embedding_loss(self, inputs1, inputs2, mode):
        self.set_mode(mode)
        embed_loss, outputs1, outputs2, neg_outputs = self.unsupervised_model.loss(inputs1, inputs2)
        return embed_loss, outputs1, outputs2, neg_outputs

    def forward_once(self, samples):
        out = self.unsupervised_model.aggregate(samples)
        out = F.normalize(out, dim=1)
        return out

    def mapping_loss_nodes(self, nodes):
        source_samples = self.unsupervised_model.sample(nodes)
        source_emb = self.forward_once(source_samples)

        target_nodes = self.source_dict[nodes]
        anchor_idxs = np.argwhere(target_nodes.cpu().numpy() >= 0).reshape(-1)
        target_nodes = target_nodes[anchor_idxs]

        source_emb = source_emb[anchor_idxs].detach()

        self.set_mode("target")
        target_samples = self.unsupervised_model.sample(target_nodes)
        self.reverse_mode()

        target_emb = self.forward_once(target_samples)

        mapping_loss = 1 - torch.mean(torch.sum(source_emb*target_emb, dim=1))
        return mapping_loss


    # def mapping_loss(self, inputs1, inputs2):
    #     try:
    #         outputs1, outputs2, _ = self.unsupervised_model(inputs1, inputs2)
    #         source_emb1, source_emb2, target_emb1, target_emb2 = self.compute_embs(inputs1, inputs2, outputs1, outputs2)
    #         if source_emb1.shape[0] == 0: #No node in dictionary train
    #             return -1
    #         source_emb = torch.cat([x for x in [source_emb1, source_emb2] if x is not None], dim=0)
    #         target_emb = torch.cat([x for x in [target_emb1, target_emb2] if x is not None], dim=0)

    #     except Exception as e:
    #         print(e)
    #         pdb.set_trace()

    #     diff = target_emb - self.map_fc(source_emb)
    #     mapping_loss = diff.norm(dim=1).mean()
    #     return mapping_loss

    # def compute_embs(self, inputs1, inputs2, source_emb1, source_emb2):
    #     target_nodes1 = self.dictionary[inputs1]
    #     target_nodes2 = self.dictionary[inputs2]
    #     anchor_idxs1 = np.argwhere(target_nodes1.cpu().numpy() >= 0).reshape(-1)
    #     anchor_idxs2 = np.argwhere(target_nodes2.cpu().numpy() >= 0).reshape(-1)
    #     target_nodes1 = target_nodes1[anchor_idxs1]
    #     target_nodes2 = target_nodes2[anchor_idxs2]

    #     source_emb1 = source_emb1[anchor_idxs1]
    #     source_emb2 = source_emb2[anchor_idxs2]

    #     prev_sample_fn = self.unsupervised_model.sample_fn
    #     self.unsupervised_model.sample_fn = self.target_sample_fn
    #     if len(target_nodes1) > 0:
    #         target_samples1 = self.unsupervised_model.sample(target_nodes1)
    #         target_emb1 = self.forward_once(target_samples1)
    #     else:
    #         target_emb1 = None
    #     if len(target_nodes2) > 0:
    #         target_samples2 = self.unsupervised_model.sample(target_nodes2)
    #         target_emb2 = self.forward_once(target_samples2)
    #     else:
    #         target_emb2 = None
    #     self.unsupervised_model.sample_fn = prev_sample_fn
    #     return source_emb1, source_emb2, target_emb1, target_emb2
