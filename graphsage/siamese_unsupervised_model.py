import torch
import torch.nn as nn
import time
import os
import numpy as np

import torch.nn.functional as F
from graphsage.neigh_samplers import UniformNeighborSampler
from graphsage.models import SampleAndAggregate
from graphsage.preps import prep_lookup
from graphsage.utils import IdentityLayer
import pdb

class SiameseUnsupervisedGraphSage(nn.Module):

    def __init__(self, unsupervised_model, source_dict, target_dict, groundtruth,
        source_features, target_features,
        source_adj, target_adj,
        source_deg, target_deg,
        map_fc="identity", mapping_loss = "cosine", embedding_loss_weight=1, mapping_loss_weight=1):

        """
        unsupervised_model: unsupervised model graphsage
        groundtruth: groundtruth dict
        target_adj: adj of the target set
        """

        super(SiameseUnsupervisedGraphSage, self).__init__()
        self.unsupervised_model = unsupervised_model
        self.embedding_dim = self.unsupervised_model.agg_layers[-1].output_dim
        self.mapping_loss = mapping_loss

        if map_fc == "identity":
            self.map_fc = IdentityLayer()
        elif map_fc == "linear":
            self.map_fc=nn.Linear(self.embedding_dim, self.embedding_dim, bias=True)
        elif map_fc == "mlp":
            self.map_fc = nn.Sequential(*[
                nn.Linear(self.embedding_dim, self.embedding_dim, bias=True),
                nn.ReLU()
            ])
        else:
            raise Exception("Unknown map function: ", map_fc)

        self.source_dict = source_dict
        self.target_dict = target_dict
        self.groundtruth = groundtruth
        self.source_features = source_features
        self.target_features = target_features
        self.source_adj = source_adj
        self.target_adj = target_adj
        self.source_deg = source_deg
        self.target_deg = target_deg
        self.source_sample_fn = UniformNeighborSampler(source_adj)
        self.target_sample_fn = UniformNeighborSampler(target_adj)
        self.embedding_loss_weight = embedding_loss_weight
        self.mapping_loss_weight = mapping_loss_weight
        self.source_prep = self.unsupervised_model.prep

        if(type(self.source_prep) == prep_lookup['use_original_features']):
            self.target_prep = prep_lookup['use_original_features'](input_dim=self.source_prep.input_dim, n_nodes=self.target_deg.shape[0])
        elif (type(self.source_prep) == prep_lookup['use_identity_features']):
            self.target_prep = prep_lookup['use_identity_features'](input_dim=self.source_prep.input_dim, n_nodes=self.target_deg.shape[0], identity_dim=self.source_prep.identity_dim)            
    
    def set_mode(self, mode):
        if mode == "source":
            self.unsupervised_model.sample_fn = self.source_sample_fn
            self.unsupervised_model.features = self.source_features
            self.unsupervised_model.train_adj = self.source_adj
            self.unsupervised_model.adj = self.source_adj
            self.unsupervised_model.train_deg = self.source_deg
            self.unsupervised_model.deg = self.source_deg
            self.unsupervised_model.prep = self.source_prep

        elif mode == "target":
            self.unsupervised_model.sample_fn = self.target_sample_fn
            self.unsupervised_model.features = self.target_features
            self.unsupervised_model.train_adj = self.target_adj
            self.unsupervised_model.adj = self.target_adj
            self.unsupervised_model.train_deg = self.target_deg
            self.unsupervised_model.deg = self.target_deg
            self.unsupervised_model.prep = self.target_prep
        else:
            raise Exception("Unsupported mode {0}".format(mode))

    def loss(self, inputs1, inputs2, mode):

        # embedding loss        
        self.set_mode(mode)
        embedding_loss, outputs1, outputs2, _ = self.unsupervised_model.loss(inputs1, inputs2)

        # mapping loss
        source_emb1, source_emb2, target_emb1, target_emb2 = self.compute_embs(inputs1, inputs2, outputs1, outputs2, mode)
        if source_emb1 is None:
            total_loss = self.embedding_loss_weight*embedding_loss
            return total_loss, embedding_loss, torch.Tensor([-1])
        else:
            source_emb = torch.cat([source_emb1, source_emb2], dim=0)
            target_emb = torch.cat([target_emb1, target_emb2], dim=0)
                        
            if self.mapping_loss == "cosine":                                
                mapping_loss = 1 - torch.mean(torch.sum(source_emb*target_emb, dim = 1))
            else:
                diff = target_emb - self.map_fc(source_emb)
                # Mean of Frobenius norm on all diff vectors
                mapping_loss = diff.norm(dim=1).mean()

            total_loss = self.embedding_loss_weight*embedding_loss + self.mapping_loss_weight*mapping_loss
            #total_loss = mapping_loss
            return total_loss, embedding_loss, mapping_loss

    def forward_once(self, samples):
        out = self.unsupervised_model.aggregate(samples)        
        out = F.normalize(out, dim=1)
        return out

    def compute_embs(self, inputs1, inputs2, source_emb1, source_emb2, mode):

        if mode == "source":
            target_nodes1 = self.source_dict[inputs1]
            target_nodes2 = self.source_dict[inputs2]
        elif mode == "target":
            target_nodes1 = self.target_dict[inputs1]
            target_nodes2 = self.target_dict[inputs2]
        else:
            raise Exception("Unsupported mode {0}".format(mode))

        anchor_idxs1 = np.argwhere(target_nodes1.cpu().numpy() >= 0).reshape(-1)
        anchor_idxs2 = np.argwhere(target_nodes2.cpu().numpy() >= 0).reshape(-1)
        target_nodes1 = target_nodes1[anchor_idxs1]
        target_nodes2 = target_nodes2[anchor_idxs2]

        source_emb1 = source_emb1[anchor_idxs1]
        source_emb2 = source_emb2[anchor_idxs2]

        if mode == "source":
            #Move to target sample fn
            self.set_mode("target")
        elif mode == "target":
            self.set_mode("source")
        else:
            raise Exception("Unsupported mode")

        target_samples1 = self.unsupervised_model.sample(target_nodes1)
        target_samples2 = self.unsupervised_model.sample(target_nodes2)

        if target_samples1[0].shape[0] == 0 or target_samples2[0].shape[0] == 0:
            return None, None, None, None

        target_emb1 = self.forward_once(target_samples1)
        target_emb2 = self.forward_once(target_samples2)

        if mode == "source":
            return source_emb1, source_emb2, target_emb1, target_emb2
        elif mode == "target":
            return target_emb1, target_emb2, source_emb1, source_emb2
