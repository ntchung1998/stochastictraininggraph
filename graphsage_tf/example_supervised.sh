time python -m graphsage.supervised_train --train_prefix $HOME/dataspace/graph/ppi/graphsage/ppi --model graphsage_mean --sigmoid --gpu 0 --batch_size 256 --epochs 100 --max_degree 25 --identity_dim 128 --learning_rate 0.001
#python -m graphsage.supervised_train --train_prefix ./example_data/cora/graphsage/cora --model graphsage_mean --batch_size 256 --epochs 20
