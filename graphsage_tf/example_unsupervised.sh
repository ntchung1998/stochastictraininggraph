time python -m graphsage.unsupervised_train --train_prefix $HOME/dataspace/graph/ppi/graphsage/ppi --model graphsage_mean --gpu 0 --batch_size 10 --epochs 100 --max_degree 25 --identity_dim 128 --max_total_steps 1
#time python -m graphsage.unsupervised_train --train_prefix ./example_data/ppi/graphsage/ppi --model graphsage_mean --gpu 0 --max_degree 25 



python -m graphsage.unsupervised_train \
--train_prefix $HOME/dataspace/graph/ppi/sub_graph/subgraph3/graphsage/ppi \
--model graphsage_mean --gpu 0 --batch_size 512 --epochs 100 \
--max_degree 25 --identity_dim 128 --random_context False --print_every 10
