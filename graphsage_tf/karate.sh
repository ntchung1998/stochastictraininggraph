OUTDIR=visualize/karate
MODEL=graphsage_mean
PREFIX=karate
DS=$HOME/dataspace/graph/${PREFIX}
TRAIN_RATIO=0.8
RATIO=0.1
LR=0.0001

############################### compare karate vs karate using vecmap ###############
source activate pytorch

# normal save to original 
time python -m graphsage.unsupervised_train --train_prefix ${DS}/graphsage/${PREFIX} --model graphsage_meanpool --model_size small \
    --epochs 100 --validate_iter 10 --print_every 10 --samples_1 8 --samples_2 4 --neg_sample_size 2 --gpu 0 --batch_size 8 --max_degree 8 --learning_rate ${LR}\
    --identity_dim 128 --base_log_dir ${OUTDIR}/tf

